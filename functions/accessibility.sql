/* Function for estimating potential spreading from a (set of) source lake(s)
based on a user defined threshold for a species ability to climb slopes in a stream
*/
-- FUNCTION: public.accessible_lakes_threshold_type(integer, smallint)

-- DROP FUNCTION public.accessible_lakes_threshold_type(integer, smallint);

CREATE OR REPLACE FUNCTION nofa.accessible_lakes_threshold_type(
	from_lake integer,
	threshold smallint,
  slope_measure text)
    RETURNS TABLE(source_lake integer, accessible_lake integer, accessibility_type character varying)
    LANGUAGE 'sql'

    COST 100
    VOLATILE
    ROWS 1000
AS $BODY$

  SELECT
      source_lake, accessible_lake, accessibility_type
  FROM
      (SELECT from_lake AS source_lake,
       to_lake AS accessible_lake,
       CASE
       WHEN upstream_slope_mean < 0 THEN 'downstreams'
       WHEN upstream_slope_mean >= 0 AND  downstream_slope_mean >= 0 THEN 'down-/upstreams'
       ELSE 'upstreams'
       END AS accessibility_type
       FROM agder.lake_connectivity
 		 WHERE
     	 wrid = (SELECT ecco_biwa_wr FROM nofa.lake WHERE "id" = $1) AND
     	 from_lake = $1 AND upstream_slope_mean <=  $2) AS uc    UNION ALL SELECT
       *
      FROM
      (SELECT to_lake AS source_lake,
        from_lake AS accessible_lake,
                CASE
       WHEN downstream_slope_mean < 0 THEN 'downstreams'
       WHEN upstream_slope_mean >= 0 AND  downstream_slope_mean >= 0 THEN 'down-/upstreams'
       ELSE 'upstreams'
       END AS accessibility_type
       FROM agder.lake_connectivity
	   WHERE
      wrid = (SELECT ecco_biwa_wr FROM nofa.lake WHERE "id" = $1) AND
      to_lake = $1 AND downstream_slope_mean <= $2) AS x

$BODY$;

ALTER FUNCTION nofa.accessible_lakes_threshold_type(integer, smallint)
    OWNER TO stefan;


/*
Function for estimating the likelihood of spreading of a user-given species
from a (set of) source lake(s), where the species ability to climb slopes in a stream
is fetched from model coefficients stored in nofa."l_taxonSlopeEffect"

FUNCTION: nofa.accessible_lakes_species(
	wrid integer,
	"waterBodyID" integer,
	"taxonID" integer);

DROP FUNCTION nofa.accessible_lakes_species(
	wrid integer,
	"waterBodyID" integer,
	"taxonID" integer);
*/

CREATE OR REPLACE FUNCTION nofa.accessible_lakes_species(
	wrid integer,
	"waterBodyID" integer,
	"taxonID" integer)
RETURNS TABLE(
	source_lake integer,
	accessible_lake integer,
	slope smallint,
	distance integer,
	total_distance integer,
	likelihood double precision) AS
$$
DECLARE
    slope_effect nofa."l_taxonSlopeEffect"%ROWTYPE;
	wrid ALIAS FOR $1;
	"waterBodyID" ALIAS FOR $2;
	taxon_id ALIAS FOR $3;
BEGIN
	SELECT * INTO slope_effect FROM nofa."l_taxonSlopeEffect" WHERE "l_taxonSlopeEffect"."taxonID" = taxon_id LIMIT 1;
	RETURN QUERY EXECUTE '
    SELECT
        source_lake,
		accessible_lake,
		slope,
		distance,
		total_distance,
        CASE
            WHEN distance <= 0 OR
                (' || slope_effect.intercept || ' + ' || slope_effect."slopeEffect" || ' * slope + (' || slope_effect."distanceEffect" || '/100.0) * log(distance)) >= 5999 OR
                (' || slope_effect.intercept || ' + ' || slope_effect."slopeEffect" || ' * slope + (' || slope_effect."distanceEffect" || '/100.0) * log(distance)) <= -5999 THEN CAST(1 AS double precision)
            ELSE CAST(round((exp(CAST(' || slope_effect.intercept || ' + ' || slope_effect."slopeEffect" || ' * (slope / 100.0) + ' || slope_effect."distanceEffect" || ' * log(distance) AS numeric)) / (1+exp(CAST(' || slope_effect.intercept || ' + ' || slope_effect."slopeEffect" || ' * (slope / 100.0) + ' || slope_effect."distanceEffect" || ' * log(distance) AS numeric)))), 12) AS double precision)
        END AS likelihood
	FROM
		(SELECT from_lake AS source_lake, to_lake AS accessible_lake, upstream_' || slope_effect."slopeMeasure" || ' AS slope, upstream_length AS distance, total_stream_length AS total_distance
 		FROM agder.lake_connectivity
    	WHERE
        	wrid = ' || wrid || ' AND
        	from_lake = ' || "waterBodyID" || ' AND upstream_' || slope_effect."slopeMeasure" ||  '< 700
		UNION ALL SELECT to_lake AS source_lake, from_lake AS accessible_lake, downstream_' || slope_effect."slopeMeasure" || ' AS slope, downstream_length AS distance, total_stream_length AS total_distance
 		FROM agder.lake_connectivity
    	WHERE
        	wrid = ' || wrid || ' AND
        	to_lake = ' || "waterBodyID" || ' AND downstream_' || slope_effect."slopeMeasure" ||  '< 700) AS con';
END
$$ LANGUAGE plpgsql;


-- FUNCTION: nofa.accessible_lakes_threshold(integer, integer, text, integer)

-- DROP FUNCTION nofa.accessible_lakes_threshold(integer, integer, text, integer);

CREATE OR REPLACE FUNCTION nofa.accessible_lakes_threshold(
	wrid integer,
	"waterBodyID" integer,
	slope_measure text,
	slope_threshold integer)
    RETURNS TABLE(source_lake integer, accessible_lake integer, access_type text, slope smallint, distance integer, total_distance integer)
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE
    ROWS 1000
AS $BODY$

DECLARE
	wrid ALIAS FOR $1;
	"waterBodyID" ALIAS FOR $2;
	slope_measure ALIAS FOR $3;
	slope_threshold ALIAS FOR $4;
BEGIN
	RETURN QUERY EXECUTE '
    SELECT
        source_lake,
		accessible_lake,
		CASE
		WHEN slope < 0 OR distance < 0 THEN CAST(''downstreams'' AS text)
		WHEN distance < total_distance THEN CAST(''down-/upstreams'' AS text)
		ELSE CAST(''upstreams'' AS text)
		END AS access_type,
		slope,
		distance,
		total_distance
	FROM
		(SELECT from_lake AS source_lake, to_lake AS accessible_lake, upstream_' || slope_measure || ' AS slope, upstream_length AS distance, total_stream_length AS total_distance
 		FROM agder.lake_connectivity
    	WHERE
        	wrid = ' || wrid || ' AND
        	from_lake = ' || "waterBodyID" || ' AND upstream_' || slope_measure ||  ' < ' || slope_threshold || '
		UNION ALL SELECT to_lake AS source_lake, from_lake AS accessible_lake, downstream_' || slope_measure || ' AS slope, downstream_length AS distance, total_stream_length AS total_distance
 		FROM agder.lake_connectivity
    	WHERE
        	wrid = ' || wrid || ' AND
        	to_lake = ' || "waterBodyID" || ' AND downstream_' || slope_measure ||  ' < ' || slope_threshold || ') AS con';
END

$BODY$;

ALTER FUNCTION nofa.accessible_lakes_threshold(integer, integer, text, integer)
    OWNER TO stefan;
