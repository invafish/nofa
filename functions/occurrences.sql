-- FUNCTION: nofa.get_last_occurrence_status(integer, text, text, text, text)

-- DROP FUNCTION nofa.get_last_occurrence_status(integer, text, text, text, text);

CREATE OR REPLACE FUNCTION nofa.get_last_occurrence_status(
	"taxonID" integer,
	reliability text DEFAULT NULL::text,
	wrids text DEFAULT NULL::text,
	countrycodes text DEFAULT NULL::text,
	counties text DEFAULT NULL::text)
    RETURNS TABLE(wrid integer, "waterBodyID" integer, "locationID" uuid, "occurrenceStatus" text, reliability character varying, "dateLast" timestamp without time zone)
    LANGUAGE 'sql'

    COST 100
    VOLATILE
    ROWS 1000
AS $BODY$

/*
    Select last registered occurrences of a species
*/
SELECT
-- There can be several locations in one waterbody
DISTINCT ON ("waterBodyID") l."eb_waterregionID", l."waterBodyID", s.* FROM
    (SELECT
        "locationID"
        -- Turn "occurrenceStatus" into binary variable
        , CASE WHEN "occurrenceStatus" = 'absent' THEN 'absent' ELSE 'present' END AS "occurrenceStatus"
        -- Show reliability
        , reliability
        -- Show date of lat registered occurrence
        , CAST("dateLast" AS timestamp)
    FROM
        (SELECT "eventID", "occurrenceStatus" FROM nofa.occurrence WHERE "taxonID" = $1) AS o
        INNER JOIN
        (SELECT
            "eventID"
            , "locationID"
            , reliability
            , "dateStart"
            , "dateEnd"
            , CASE WHEN "dateEnd" IS NULL THEN "dateStart" ELSE "dateEnd" END AS "dateLast"
        FROM
            nofa.event
        WHERE
            ("dateStart" IS NOT NULL OR "dateEnd" IS NOT NULL)
            AND
            -- There are several records where reliability IS NULL (currently excluded, OK?)
            (reliability IN (SELECT unnest(string_to_array($2, ','))) OR $2 IS NULL) -- 'high,medium' array as function input???
        ) AS e USING("eventID")
    ) AS s
NATURAL INNER JOIN
    (SELECT "locationID", "waterBodyID", "eb_waterregionID"
    FROM
        nofa.location
    WHERE
		-- Use rather WRID for filetering?
		("eb_waterregionID" IN (SELECT unnest(string_to_array($3, ','))::integer) OR $3 IS NULL) AND
        ("countryCode" IN (SELECT unnest(string_to_array($4, ','))) OR $4 IS NULL) AND -- 'NO,SE' array as function input???
        (county IN (SELECT unnest(string_to_array($5, ','))) OR $5 IS NULL) -- 'Aust-Agder,Vest-Agder' array as function input???
    ) AS l
ORDER BY "waterBodyID", "dateLast" DESC

$BODY$;

ALTER FUNCTION nofa.get_last_occurrence_status(integer, text, text, text, text)
    OWNER TO stefan;
