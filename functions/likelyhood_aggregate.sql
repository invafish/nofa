/* product() functions for accumulating likelyhood estimates of independent events
e.g. likelyhood of introduction of a species from different source lakes or
accumulated likelyhood over several independent simulations of human introductions

Usage example:
-- Compute likelyhood
SELECT
  "waterBodyID"
  , nofa.product(1 - likelyhood_introduction)
FROM
  (SELECT *, random() AS likelyhood_introduction FROM
	  (SELECT generate_series (1, 10) AS "waterBodyID") AS l,
	  (SELECT generate_series (1, 10) AS "introductionEvent") AS e
  ) AS a
GROUP BY "waterBodyID";

*/

CREATE AGGREGATE nofa.product(double precision) ( SFUNC = float8mul, STYPE=double precision );
CREATE AGGREGATE nofa.product(bigint) ( SFUNC = int8mul, STYPE=bigint );
CREATE AGGREGATE nofa.product(integer) ( SFUNC = int4mul, STYPE=integer );
