--
-- PostgreSQL database dump
--

-- Dumped from database version 10.10 (Ubuntu 10.10-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 10.10 (Ubuntu 10.10-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: nofa; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA "nofa";


--
-- Name: SCHEMA "nofa"; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON SCHEMA "nofa" IS 'Schema holding the Nordic Fish Atlas (NOFA). This is the main table of the NOFA database with primary data and waterbody geometries and identifiers. Please note that secondary environmental data and convinience tables or views for dataextactions should go to other schemas.  Please see https://github.com/ninanor/nofa/wiki for further documentation.';


--
-- Name: plugin; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA "plugin";


--
-- Name: accessible_lakes_species(integer, integer, integer); Type: FUNCTION; Schema: nofa; Owner: -
--

CREATE FUNCTION "nofa"."accessible_lakes_species"("wrid" integer, "waterBodyID" integer, "taxonID" integer) RETURNS TABLE("source_lake" integer, "accessible_lake" integer, "slope" smallint, "distance" integer, "total_distance" integer, "likelihood" double precision)
    LANGUAGE "plpgsql"
    AS $_$

DECLARE
    slope_effect nofa."l_taxonSlopeEffect"%ROWTYPE;
	wrid ALIAS FOR $1;
	"waterBodyID" ALIAS FOR $2;
	taxon_id ALIAS FOR $3;
BEGIN
	SELECT * INTO slope_effect FROM nofa."l_taxonSlopeEffect" WHERE "l_taxonSlopeEffect"."taxonID" = taxon_id LIMIT 1;
	RETURN QUERY EXECUTE '
    SELECT
        source_lake,
		accessible_lake,
		slope,
		distance,
		total_distance,
        CASE
            WHEN distance <= 0 OR
                (' || slope_effect.intercept || ' + ' || slope_effect."slopeEffect" || ' * slope + (' || slope_effect."distanceEffect" || '/100.0) * log(distance)) >= 5999 OR
                (' || slope_effect.intercept || ' + ' || slope_effect."slopeEffect" || ' * slope + (' || slope_effect."distanceEffect" || '/100.0) * log(distance)) <= -5999 THEN CAST(1 AS double precision)
            ELSE CAST(round((exp(CAST(' || slope_effect.intercept || ' + ' || slope_effect."slopeEffect" || ' * (slope / 100.0) + ' || slope_effect."distanceEffect" || ' * log(distance) AS numeric)) / (1+exp(CAST(' || slope_effect.intercept || ' + ' || slope_effect."slopeEffect" || ' * (slope / 100.0) + ' || slope_effect."distanceEffect" || ' * log(distance) AS numeric)))), 12) AS double precision)
        END AS likelihood
	FROM
		(SELECT from_lake AS source_lake, to_lake AS accessible_lake, upstream_' || slope_effect."slopeMeasure" || ' AS slope, upstream_length AS distance, total_stream_length AS total_distance
 		FROM agder.lake_connectivity
    	WHERE
        	wrid = ' || wrid || ' AND
        	from_lake = ' || "waterBodyID" || ' AND upstream_' || slope_effect."slopeMeasure" ||  '< 5500
		UNION ALL SELECT to_lake AS source_lake, from_lake AS accessible_lake, downstream_' || slope_effect."slopeMeasure" || ' AS slope, downstream_length AS distance, total_stream_length AS total_distance
 		FROM agder.lake_connectivity
    	WHERE
        	wrid = ' || wrid || ' AND
        	to_lake = ' || "waterBodyID" || ' AND downstream_' || slope_effect."slopeMeasure" ||  '< 5500) AS con';
END

$_$;


--
-- Name: accessible_lakes_threshold(integer, integer, "text", integer); Type: FUNCTION; Schema: nofa; Owner: -
--

CREATE FUNCTION "nofa"."accessible_lakes_threshold"("wrid" integer, "waterBodyID" integer, "slope_measure" "text", "slope_threshold" integer) RETURNS TABLE("source_lake" integer, "accessible_lake" integer, "access_type" "text", "slope" smallint, "distance" integer, "total_distance" integer)
    LANGUAGE "plpgsql"
    AS $_$
DECLARE
	wrid ALIAS FOR $1;
	"waterBodyID" ALIAS FOR $2;
	slope_measure ALIAS FOR $3;
	slope_threshold ALIAS FOR $4;
BEGIN
	RETURN QUERY EXECUTE '
    SELECT
        source_lake,
		accessible_lake,
		CASE
		WHEN slope < 0 OR distance < 0 THEN CAST(''downstreams'' AS text)
		WHEN distance < total_distance THEN CAST(''down-/upstreams'' AS text)
		ELSE CAST(''upstreams'' AS text)
		END AS access_type,
		slope,
		distance,
		total_distance
	FROM
		(SELECT from_lake AS source_lake, to_lake AS accessible_lake, upstream_' || slope_measure || ' AS slope, upstream_length AS distance, total_stream_length AS total_distance
 		FROM agder.lake_connectivity
    	WHERE
        	wrid = ' || wrid || ' AND
        	from_lake = ' || "waterBodyID" || ' AND upstream_' || slope_measure ||  ' < ' || slope_threshold || '
		UNION ALL SELECT to_lake AS source_lake, from_lake AS accessible_lake, downstream_' || slope_measure || ' AS slope, downstream_length AS distance, total_stream_length AS total_distance
 		FROM agder.lake_connectivity
    	WHERE
        	wrid = ' || wrid || ' AND
        	to_lake = ' || "waterBodyID" || ' AND downstream_' || slope_measure ||  ' < ' || slope_threshold || ') AS con';
END
$_$;


--
-- Name: get_last_occurrence_status(integer, "text", "text", "text", "text"); Type: FUNCTION; Schema: nofa; Owner: -
--

CREATE FUNCTION "nofa"."get_last_occurrence_status"("taxonID" integer, "reliability" "text" DEFAULT NULL::"text", "wrids" "text" DEFAULT NULL::"text", "countrycodes" "text" DEFAULT NULL::"text", "counties" "text" DEFAULT NULL::"text") RETURNS TABLE("wrid" integer, "waterBodyID" integer, "locationID" "uuid", "occurrenceStatus" "text", "reliability" character varying, "dateLast" timestamp without time zone)
    LANGUAGE "sql"
    AS $_$

/*
    Select last registered occurrences of a species
*/
SELECT
-- There can be several locations in one waterbody
DISTINCT ON ("waterBodyID") l."eb_waterregionID", l."waterBodyID", s.* FROM 
    (SELECT
        "locationID"
        -- Turn "occurrenceStatus" into binary variable
        , CASE WHEN "occurrenceStatus" = 'absent' THEN 'absent' ELSE 'present' END AS "occurrenceStatus"
        -- Show reliability
        , reliability
        -- Show date of lat registered occurrence
        , CAST("dateLast" AS timestamp)
    FROM 
        (SELECT "eventID", "occurrenceStatus" FROM nofa.occurrence WHERE "taxonID" = $1) AS o
        INNER JOIN 
        (SELECT
            "eventID"
            , "locationID"
            , reliability
            , "dateStart"
            , "dateEnd"
            , CASE WHEN "dateEnd" IS NULL THEN "dateStart" ELSE "dateEnd" END AS "dateLast"
        FROM
            nofa.event
        WHERE 
            ("dateStart" IS NOT NULL OR "dateEnd" IS NOT NULL)
            AND
            -- There are several records where reliability IS NULL (currently excluded, OK?)
            (reliability IN (SELECT unnest(string_to_array($2, ','))) OR $2 IS NULL) -- 'high,medium' array as function input???
        ) AS e USING("eventID")
    ) AS s
NATURAL INNER JOIN
    (SELECT "locationID", "waterBodyID", "eb_waterregionID"
    FROM
        nofa.location
    WHERE
		-- Use rather WRID for filetering?
		("eb_waterregionID" IN (SELECT unnest(string_to_array($3, ','))::integer) OR $3 IS NULL) AND
        ("countryCode" IN (SELECT unnest(string_to_array($4, ','))) OR $4 IS NULL) AND -- 'NO,SE' array as function input???
        (county IN (SELECT unnest(string_to_array($5, ','))) OR $5 IS NULL) -- 'Aust-Agder,Vest-Agder' array as function input???
    ) AS l
ORDER BY "waterBodyID", "dateLast" DESC

$_$;


--
-- Name: get_last_occurrence_status_wb(integer, "text", "text", "text", "text", "text"); Type: FUNCTION; Schema: nofa; Owner: -
--

CREATE FUNCTION "nofa"."get_last_occurrence_status_wb"("taxonID" integer, "reliability" "text" DEFAULT NULL::"text", "wrids" "text" DEFAULT NULL::"text", "countrycodes" "text" DEFAULT NULL::"text", "counties" "text" DEFAULT NULL::"text", "waterbodies" "text" DEFAULT NULL::"text") RETURNS TABLE("taxonID" integer, "wrid" integer, "waterBodyID" integer, "locationID" "uuid", "occurrenceStatus" "text", "reliability" character varying, "dateLast" timestamp without time zone)
    LANGUAGE "sql"
    AS $_$

/*
    Select last registered occurrences of a species
*/
SELECT
-- There can be several locations in one waterbody
DISTINCT ON ("waterBodyID") CAST($1 AS integer) AS "taxonID", l."eb_waterregionID", l."waterBodyID", s.* FROM 
    (SELECT
        "locationID"
        -- Turn "occurrenceStatus" into binary variable
        , CASE WHEN "occurrenceStatus" = 'absent' THEN 'absent' ELSE 'present' END AS "occurrenceStatus"
        -- Show reliability
        , reliability
        -- Show date of lat registered occurrence
        , CAST("dateLast" AS timestamp)
    FROM 
        (SELECT "eventID", "occurrenceStatus" FROM nofa.occurrence WHERE "taxonID" = $1) AS o
        INNER JOIN 
        (SELECT
            "eventID"
            , "locationID"
            , reliability
            , "dateStart"
            , "dateEnd"
            , CASE WHEN "dateEnd" IS NULL THEN "dateStart" ELSE "dateEnd" END AS "dateLast"
        FROM
            nofa.event
        WHERE 
            ("dateStart" IS NOT NULL OR "dateEnd" IS NOT NULL)
            AND
            -- There are several records where reliability IS NULL (currently excluded, OK?)
            (reliability IN (SELECT unnest(string_to_array($2, ','))) OR $2 IS NULL) -- 'high,medium' array as function input???
        ) AS e USING("eventID")
    ) AS s
NATURAL INNER JOIN
    (SELECT "locationID", "waterBodyID", "eb_waterregionID"
    FROM
        nofa.location
    WHERE
		-- Use rather WRID for filetering?
		("eb_waterregionID" IN (SELECT unnest(string_to_array($3, ','))::integer) OR $3 IS NULL) AND
        ("countryCode" IN (SELECT unnest(string_to_array($4, ','))) OR $4 IS NULL) AND -- 'NO,SE' array as function input???
        (county IN (SELECT unnest(string_to_array($5, ','))) OR $5 IS NULL) AND -- 'Aust-Agder,Vest-Agder' array as function input???
        ("waterBodyID" IN (SELECT CAST(unnest(string_to_array($6, ',')) AS integer)) OR $6 IS NULL) -- 'Aust-Agder,Vest-Agder' array as function input???
    ) AS l
ORDER BY "waterBodyID", "dateLast" DESC

$_$;


--
-- Name: match_location2waterbody(); Type: FUNCTION; Schema: nofa; Owner: -
--

CREATE FUNCTION "nofa"."match_location2waterbody"() RETURNS "trigger"
    LANGUAGE "plpgsql"
    AS $$
DECLARE
location_type text;
location_geom geometry;
tmp_row record; --necessary for accessing different tables TODO make it nicer
BEGIN
-- ASSIGN Location TYpe
location_type = NEW."locationType";
IF
    location_type IN ('lake centroid',
                  'lake outlet')
    THEN
    FOR tmp_row IN
        (SELECT *FROM match_lake(NEW.geom))
    LOOP
        NEW."matching_remark":= tmp_row.remark;
        NEW."waterBodyID":= tmp_row.id;
END LOOP;
--ELSEIF --> other cases will follow ...
END IF;
RETURN NEW;
END;
$$;


--
-- Name: FUNCTION "match_location2waterbody"(); Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON FUNCTION "nofa"."match_location2waterbody"() IS '
Matches locations to waterbodies like lakes, rivers and other waterbodies. 
';


--
-- Name: updatecentroid(); Type: FUNCTION; Schema: nofa; Owner: -
--

CREATE FUNCTION "nofa"."updatecentroid"() RETURNS "trigger"
    LANGUAGE "plpgsql"
    AS $$
BEGIN
  IF NEW.centroid IS NULL
  THEN
    NEW.centroid = ST_PointOnSurface(NEW.geom);
  END IF;
  RETURN NEW;
END;
$$;


--
-- Name: updatedecimallatlon(); Type: FUNCTION; Schema: nofa; Owner: -
--

CREATE FUNCTION "nofa"."updatedecimallatlon"() RETURNS "trigger"
    LANGUAGE "plpgsql"
    AS $$
DECLARE
    geom_column text;
BEGIN
  CASE TG_ARGV[0]
    WHEN 'centroid' THEN
          geom_column = NEW.centroid;
    WHEN 'geom' THEN
          geom_column = NEW.geom;
    end CASE;
  geom_column = ST_Transform(geom_column, 4326);
  IF NEW.decimalLongitude IS NULL
  THEN
    -- TODO convert to 4326
    NEW.decimalLongitude = ST_X(geom_column);
  END IF;
  IF NEW.decimalLatitude IS NULL
  THEN
    NEW.decimalLatitude = ST_Y(geom_column);
  END IF;
  raise notice 'New is: %', NEW;
  RETURN NEW;
END;
$$;


--
-- Name: product(double precision); Type: AGGREGATE; Schema: nofa; Owner: -
--

CREATE AGGREGATE "nofa"."product"(double precision) (
    SFUNC = "float8mul",
    STYPE = double precision
);


--
-- Name: product(integer); Type: AGGREGATE; Schema: nofa; Owner: -
--

CREATE AGGREGATE "nofa"."product"(integer) (
    SFUNC = "int4mul",
    STYPE = integer
);


--
-- Name: product(bigint); Type: AGGREGATE; Schema: nofa; Owner: -
--

CREATE AGGREGATE "nofa"."product"(bigint) (
    SFUNC = "int8mul",
    STYPE = bigint
);


SET default_with_oids = false;

--
-- Name: event; Type: TABLE; Schema: nofa; Owner: -
--

CREATE TABLE "nofa"."event" (
    "eventRemarks" "text",
    "recordedBy" character varying(50),
    "referenceID" integer,
    "projectID" integer,
    "impRef" character varying(500),
    "samplingTaxaRange" integer[],
    "dateStart" "date",
    "dateEnd" "date",
    "samplingEffort" integer,
    "sampleSizeUnit" character varying(30),
    "sampleSizeValue" numeric(10,1),
    "samplingProtocol" character varying(100),
    "reliability" character varying(50),
    "fieldNumber" character varying(255),
    "fieldNotes" "text",
    "datasetID" character varying(255),
    "eventID" "uuid" DEFAULT "public"."uuid_generate_v4"() NOT NULL,
    "locationID" "uuid" NOT NULL,
    "modified" timestamp without time zone,
    "parentEventID" "uuid",
    "eventDateQualifier" character varying(50),
    "minimumDepthInMeters" double precision,
    "maximumDepthInMeters" double precision
);


--
-- Name: TABLE "event"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON TABLE "nofa"."event" IS 'The nofa.event table mimics the event class of terms from DwC (i.e. describes an action that occurs at some location during some time). Events are, hence, described by a location (in the event table represented by a link to the location table through locationID), a time intervall which the event occurred (defined by dateStart and dateEnd), and the type of action (defined by samplingProtocol). 

Each event is uniquely defined by its eventID (a UUID type of field: https://en.wikipedia.org/wiki/Universally_unique_identifier), and the more human readable term fieldNumber (intended to provide a link back to orginal data). Events can be nested within each other (e.g. a gill-net chain event can consist data recorded on several indivdiual gillnets). The nesting is described by a recursive datamodel, where each child event is identified back to its parent using the parentEventID (UUID). At the moment there are no human readable field describing parent events. One could consider introducing a term parentFieldNumber for this puropose.  

In addition sampling details are given (samplingEffort, sampeSizeUnit, sampleSizeValue), and also information about accessRights and owner properties (accessRigths, institutionCode, rightsHolder). ';


--
-- Name: COLUMN "event"."eventRemarks"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."event"."eventRemarks" IS 'NOFA: Use this field for indicating in plain text anything about the sampling. Note that if there is something going on that has some implication for the reliability of the data originating from the event, this should be followed up by marking in the reliability field (see reliabilityID comments for codes). However, this is a good place for making more descriptive comments. (e.g. “A.G. Finstad was messing up the depth annotation of the gill-nets due to general sloppiness. Maximum and minimum depth carefully reconstructed by the student”).    | Comments or notes about the sampling event.  | Refference: http://rs.tdwg.org/dwc/terms/index.htm#eventRemarks';


--
-- Name: COLUMN "event"."recordedBy"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."event"."recordedBy" IS 'NOFA: A vertical bar (‘|’) separated lists of people groups or institutions responsible for the collection event. In case of original field data this could consist of the personnel doing the field sampling or the institution in charge. Note that literature surveys (i.e. data harvested from literature or other written sources) this field should indicate the original personnel or institution doing the sampling (if known). In case of interview surveys, this field should indicate the name of the personnel or institution doing the interview (e.g. Trygve Hesthagen, NINA). Note that the identity of interview subjects not normally should be stored in NOFA due to security reasons. Exceptions are when the interview subjects are representing official institutions or organizations (e.g.  “County Governor of Nord-Trøndelag, by Anton Rikstad”).  | DwC: A list (concatenated and separated) of names of people, groups, or organizations responsible for recording the original Occurrence. The primary collector or observer, especially one who applies a personal identifier (recordNumber), should be listed first.  | Refference: http://rs.tdwg.org/dwc/terms/recordedBy';


--
-- Name: COLUMN "event"."referenceID"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."event"."referenceID" IS 'NOFA: An ID to the refference of the information defined as “A related resource that is referenced, cited, or otherwise pointed to by the described resource.”. Used here as a foregin key to the m_refference table wher further information on the refference can be found. Idealy, this should be a resolvable identifier in the form of a DOI or stable URL  if existing (e.g. permanent URL to Huitfeldt-Kaas 1918 in the National Library of Norway is  http://urn.nb.no/URN:NBN:no-nb_digibok_2006120500031). The NOFA term referenceID should probalby be replaced by the DwC term reference. | DC: (usage equivalent to “dc:reference”). A related resource that is referenced, cited, or otherwise pointed to by the described resource. | Refference: http://purl.org/dc/terms/references';


--
-- Name: COLUMN "event"."projectID"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."event"."projectID" IS 'NOFA: A reference (ID) to the project gathering data. Ideally this should also be a self-resolving identifier e.g. in the form of a stabel URL. However, in lack of that, this is a foreign key to the m_project table giving further information about the project. Note that the projectID and associated information first have to be entered into the m_project table.  | NOFA unique term | Refference: ';


--
-- Name: COLUMN "event"."impRef"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."event"."impRef" IS 'NOFA: Import reference for data. Used for database maintenance and backward compatibility. In the data-set “NoFa_occurrence” this indicates the running number from the “forekomst database” from which the data-set originally was imported.  | NOFA unique term | Refference: ';


--
-- Name: COLUMN "event"."samplingTaxaRange"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."event"."samplingTaxaRange" IS 'NOFA: A list of taxonID’s associated with the sampling, defined as the taxa that may be found/identified given the taxonomic competence of the personnel involved in the sampling or subsequent identification and the samplingProtocol employed. Note it may be convenient to use higher taxa for some sampling protocols.  | DwC: suggested term | Refference: ';


--
-- Name: COLUMN "event"."dateStart"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."event"."dateStart" IS 'NOFA: Start date of the sampling event (in ISO format  https://no.wikipedia.org/wiki/ISO_8601). NB, in case of an interval is not defined/known leave this field blank and enter only a endDate. The logic behind this is that the end Date is the most corresponding to the date of the known observation (i.e. the date at which the given occurrence of an organism was registered). Note that this form of entering is a convenience wrapper for the DwC eventDate which is defined as an interval, but harder to handle both in terms of database maintenance and data input.  | Start date of the sampling event (in ISO format  https://no.wikipedia.org/wiki/ISO_8601). NB, in case of an interval is not defined/known leave this field blank and enter only a endDate. The logic behind this is that the end Date is the most corresponding to the date of the known observation (i.e. the date at which the given occurrence of an organism was registered). Note that this form of entering is a convenience wrapper for the DwC eventDate which is defined as an interval, but harder to handle both in terms of database maintenance and data input.  | Refference: http://rs.tdwg.org/dwc/terms/eventDate';


--
-- Name: COLUMN "event"."dateEnd"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."event"."dateEnd" IS 'NOFA: End date of the sampling event (in ISO format  https://no.wikipedia.org/wiki/ISO_8601). NB, in case of an interval is not defined/known use this field to enter the date of the observation. The logic behind this is that the end Date is the most corresponding to the date of the known observation (i.e. the date at which the given occurrence of an organism was registered). Note that this form of entering is a convenience wrapper for the DwC eventDate which is defined as an interval, but harder to handle both in terms of database maintenance and data input.  | Start date of the sampling event (in ISO format  https://no.wikipedia.org/wiki/ISO_8601). NB, in case of an interval is not defined/known leave this field blank and enter only a endDate. The logic behind this is that the end Date is the most corresponding to the date of the known observation (i.e. the date at which the given occurrence of an organism was registered). Note that this form of entering is a convenience wrapper for the DwC eventDate which is defined as an interval, but harder to handle both in terms of database maintenance and data input.  | Refference: http://rs.tdwg.org/dwc/terms/eventDate';


--
-- Name: COLUMN "event"."samplingEffort"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."event"."samplingEffort" IS 'The number of replicated sampling units underlying the event. Ideally we want this to be 1 (i.e. the sampling event is the atomic level of the data). However, often this is not recorded in the field-notes or available data and the event contain information from several sub-events not recorded. Example: the event aggregate data from either 8 gill-nets, 8 plankton hauls or 8 funnel traps. samplingEffort is then 8. The samplingEffort is hence seen in relation to the sampling protocol and can be very important in application of the data (e.g. used as offset in a poison regression model). Note other quantities about the "sampligEffort" such as gill-net area or volume of trawl should go to the sampleSizeValue and sampleSizeUnit fields.

The NOFA definitions is hence narrower than the DwC usage of the field: "The amount of effort expended during an event". http://rs.tdwg.org/dwc/terms/samplingEfforth';


--
-- Name: COLUMN "event"."reliability"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."event"."reliability" IS 'NOFA: An indicator of the reliability status of the observations generated by the event. Controlled vocubulary found in the l_reliability lookup table. Ranging from “High reliability” generally given to professionally survey data (such as your own…) to “Not trustworthy” generally given to observations that are known to be wrong | NOFA unique term, inherited from PIKE | Refference: ';


--
-- Name: COLUMN "event"."fieldNotes"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."event"."fieldNotes" IS 'Should be removed? Uncertain on the meaning and use of this column';


--
-- Name: COLUMN "event"."parentEventID"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."event"."parentEventID" IS 'NOFA: An identifier pointing back to the eventID of the higher hierarchical event (e.g. if you choose to enter an own event to all gillnets in a chain of gillnet, lets say with eventID 240e9903-9569-4b75-9d35-9a96da40a635, with sampling protocol survey_gillnet_series_jensen_bottom, then the parentEventID field of each of the underlying events describing  each gillnet would enter  240e9903-9569-4b75-9d35-9a96da40a635. See also event table comments for further explanation.  | DwC: An identifier for the broader Event that groups this and potentially other Events. | Refference: http://rs.tdwg.org/dwc/terms/parentEventID';


--
-- Name: l_taxonSlopeEffect; Type: TABLE; Schema: nofa; Owner: -
--

CREATE TABLE "nofa"."l_taxonSlopeEffect" (
    "taxonID" integer NOT NULL,
    "slopeMeasure" character varying(250),
    "intercept" double precision,
    "slopeEffect" double precision,
    "distanceEffect" double precision
);


--
-- Name: TABLE "l_taxonSlopeEffect"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON TABLE "nofa"."l_taxonSlopeEffect" IS 'Lookup table for effect of slopes on fish dispersal ability; takes results from Logisticregressions perfomed by Sam Perrin';


--
-- Name: COLUMN "l_taxonSlopeEffect"."taxonID"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."l_taxonSlopeEffect"."taxonID" IS 'Refferences the taxonID in the l_taxon table. To as large a degree as possible this follows the taxonID from artsdatabanken (matches are positive integers). Note: resolves to human readable webpage by http://data.artsdatabanken.no/Taxon/"taxonID". Taxa not found in artsdatabankens artsnavnebase have negeative integers. DwC definition: An identifier for the set of taxon information (data associated with the Taxon class). May be a global unique identifier or an identifier specific to the data set.';


--
-- Name: COLUMN "l_taxonSlopeEffect"."slopeMeasure"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."l_taxonSlopeEffect"."slopeMeasure" IS 'Refers to the most relevant slope measure in the connectivity table for the given species.';


--
-- Name: COLUMN "l_taxonSlopeEffect"."intercept"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."l_taxonSlopeEffect"."intercept" IS 'Represents the intercept of the logistic regression of slope effect on the given species.';


--
-- Name: COLUMN "l_taxonSlopeEffect"."slopeEffect"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."l_taxonSlopeEffect"."slopeEffect" IS 'Represents the effect of the respective slope measure in the logistic regression of slope effect on the given species.';


--
-- Name: COLUMN "l_taxonSlopeEffect"."distanceEffect"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."l_taxonSlopeEffect"."distanceEffect" IS 'Represents the effect of distance in the logistic regression of slope effect on the given species.';


--
-- Name: waterBody; Type: TABLE; Schema: nofa; Owner: -
--

CREATE TABLE "nofa"."waterBody" (
    "id" integer NOT NULL,
    "nationalWaterBodyID" "text"[],
    "waterbody" character varying(35),
    "synonyms" character varying(50)[],
    "municipalityIDs" integer[],
    "county" character varying(35),
    "locality" character varying(35),
    "isBorder" boolean DEFAULT false,
    "countryCode" character varying[],
    "lastGeomUpdate" integer,
    "georeferenceSources" character varying(255),
    "georeferenceVerificationStatus" character varying(50),
    "georeferenceProtocol" "text",
    "georeferenceRemarks" "text",
    "locationRemarks" "text",
    "decimalLatitude" double precision,
    "decimalLongitude" double precision,
    "centroid" "public"."geometry"(Point,25833),
    "coordinateUncertaintyInMeters" smallint,
    "checkComment" character varying(100),
    "toBeChecked" smallint,
    "impRef" character varying(50),
    "EURBDCode" character varying(20),
    "EURBDCodes" character varying(20)[],
    "EUSubUnitC" character varying(20),
    "EUSubUnitCs" character varying(20)[],
    "ecco_biwa_wr" integer,
    "ecco_biwa_wrs" integer[]
);


--
-- Name: COLUMN "waterBody"."nationalWaterBodyID"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."waterBody"."nationalWaterBodyID" IS 'NOFA: Waterbody identification number from offical source using coding from NVE (Norway), SMHI (Sweden), SYKE (Finland) | EEA: National identification code of water body (if available) in which station is located | Refference:  http://dd.eionet.europa.eu/dataelements/latest/WaterBodyID';


--
-- Name: COLUMN "waterBody"."waterbody"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."waterBody"."waterbody" IS 'NOFA usage: waterBody name from national mapping authorities | DwC: The name of the water body in which the Location occurs. Recommended best practice is to use a controlled vocabulary such as the Getty Thesaurus of Geographic Names | Refference: http://rs.tdwg.org/dwc/terms/waterBody';


--
-- Name: lake; Type: TABLE; Schema: nofa; Owner: -
--

CREATE TABLE "nofa"."lake" (
    "lakeID" integer,
    "ebint" integer,
    "area_km2" double precision,
    "elevation" double precision,
    "bathymetryAvailable" boolean,
    "geom" "public"."geometry"(Polygon,25833),
    "centroid_latlong" "public"."geometry"(Point,4326),
    "perimeter_m" integer,
    "distance_to_road" integer,
    "municipalities" character varying[],
    "counties" character varying[],
    "countryCodes" character varying[]
)
INHERITS ("nofa"."waterBody");


--
-- Name: COLUMN "lake"."perimeter_m"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."lake"."perimeter_m" IS 'Perimeter in meter. It describes the length of the boundary of both the lake itself and the boundary of any island';


--
-- Name: location; Type: TABLE; Schema: nofa; Owner: -
--

CREATE TABLE "nofa"."location" (
    "locationID" "uuid" DEFAULT "public"."uuid_generate_v4"() NOT NULL,
    "waterBodyID" integer,
    "locationType" character varying(50) NOT NULL,
    "countryCode" character(2),
    "county" character varying(25),
    "municipality" character varying(25),
    "waterBody" character varying(50),
    "locationRemarks" "text",
    "no_vatn_lnr" integer,
    "se_sjoid" character varying(50),
    "se_lake_id" character varying(50),
    "fi_nro" character varying(25),
    "ebint" integer,
    "minimumElevationInMeters" double precision,
    "maximumElevationInMeters" double precision,
    "geodeticDatum" character varying(50),
    "georeferenceSources" "text",
    "georeferenceRemarks" "text",
    "decimalLongitude" double precision,
    "decimalLatitude" double precision,
    "coordinateUncertaintyInMeters" double precision,
    "geom" "public"."geometry"(MultiPoint,25833),
    "eb_waterregionID" integer,
    "matching_remark" "text",
    "verbatimLocality" "text",
    CONSTRAINT "geom_not_null" CHECK (("geom" IS NOT NULL))
);


--
-- Name: COLUMN "location"."locationID"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."location"."locationID" IS 'Reffence to a sampling location. May reffer to a point within a lake or stream, or to a whole waterbody if that is the resolution of the data. In the latter case, this would be indicated by the field locationType beeing set to "waterbody centroid" or "waterbody outlet" meaning that the coordinates given for this locationID is refferencing the waterbody centroid (middle point) or the waterbody (lake) outlet (in case of Swedish lakes, where this waterbody outlet is used by SMHI as official lake coordinates). 

Dwc definition: An identifier for the set of location information (data associated with dcterms:Location). May be a global unique identifier or an identifier specific to the data set. http://terms.tdwg.org/wiki/dwc:locationID. ';


--
-- Name: COLUMN "location"."waterBodyID"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."location"."waterBodyID" IS 'An internal identifier for the waterbody (lake or stream) in NOFA';


--
-- Name: COLUMN "location"."locationType"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."location"."locationType" IS 'Type of the location as defined in l_locationType';


--
-- Name: COLUMN "location"."no_vatn_lnr"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."location"."no_vatn_lnr" IS 'Norwegian waterbody ID "vatn_lnr" as assigned by NVE';


--
-- Name: COLUMN "location"."se_sjoid"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."location"."se_sjoid" IS 'Swedish waterbody ID "SJOID" as assigned by SMHI';


--
-- Name: COLUMN "location"."se_lake_id"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."location"."se_lake_id" IS 'Swedish waterbody ID "lake_id" as used in the PIKE database';


--
-- Name: COLUMN "location"."fi_nro"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."location"."fi_nro" IS 'Finish waterbody ID "nro" as used in http://rajapinnat.ymparisto.fi/api/jarvirajapinta/1.0/odata/Jarvi';


--
-- Name: COLUMN "location"."ebint"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."location"."ebint" IS 'Integer waterbody ID "ebint" as used by the ECCO/BIWA project';


--
-- Name: COLUMN "location"."geom"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."location"."geom" IS '(Multi.) Point geometry in EPSG:25833, representing the location';


--
-- Name: occurrence; Type: TABLE; Schema: nofa; Owner: -
--

CREATE TABLE "nofa"."occurrence" (
    "establishmentRemarks" character varying(255),
    "occurrenceRemarks" character varying(255),
    "verifiedDate" "date",
    "verifiedBy" character varying(50),
    "impRef" character varying(500),
    "organismQuantity" numeric(10,2),
    "organismQuantityType" character varying(50),
    "individualCount" integer,
    "occurrenceStatus" character varying(50) DEFAULT 'present'::character varying,
    "establishmentMeans" character varying(50) DEFAULT 'unknown'::character varying,
    "spawningCondition" character varying(50) DEFAULT 'unknown'::character varying,
    "spawningLocation" character varying(50) DEFAULT 'unknown'::character varying,
    "sex" character varying(50),
    "lifeStage" character varying(50),
    "reproductiveCondition" character varying(50),
    "recordNumber" character varying(255),
    "eventID" "uuid" NOT NULL,
    "occurrenceID" "uuid" DEFAULT "public"."uuid_generate_v4"() NOT NULL,
    "fieldNumber" character varying(250),
    "modified" timestamp without time zone,
    "taxonID" integer,
    "ecotypeID" integer,
    "populationTrend" character varying(50) DEFAULT 'unknown'::character varying
);


--
-- Name: COLUMN "occurrence"."taxonID"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."occurrence"."taxonID" IS 'Refferences the taxonID in the l_taxon table. To as large a degree as possible this follows the taxonID from artsdatabanken (matches are positive integers). Note: resolves to human readable webpage by http://data.artsdatabanken.no/Taxon/"taxonID". Taxa not found in artsdatabankens artsnavnebase have negeative integers. DwC definition: An identifier for the set of taxon information (data associated with the Taxon class). May be a global unique identifier or an identifier specific to the data set.';


--
-- Name: m_project; Type: TABLE; Schema: nofa; Owner: -
--

CREATE TABLE "nofa"."m_project" (
    "projectID" integer NOT NULL,
    "projectName" character varying(100),
    "projectNumber" integer,
    "costs" integer,
    "startYear" integer,
    "endYear" integer,
    "projectLeader" character varying(50),
    "financer" character varying(50),
    "projectMembers" character varying(255),
    "organisation" character varying(50),
    "remarks" character varying(10485760)
);


--
-- Name: a_prosjekt_prosjekt_id_seq; Type: SEQUENCE; Schema: nofa; Owner: -
--

CREATE SEQUENCE "nofa"."a_prosjekt_prosjekt_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: a_prosjekt_prosjekt_id_seq; Type: SEQUENCE OWNED BY; Schema: nofa; Owner: -
--

ALTER SEQUENCE "nofa"."a_prosjekt_prosjekt_id_seq" OWNED BY "nofa"."m_project"."projectID";


--
-- Name: m_reference; Type: TABLE; Schema: nofa; Owner: -
--

CREATE TABLE "nofa"."m_reference" (
    "referenceID" integer NOT NULL,
    "source" character varying(50),
    "author" character varying(255),
    "year" smallint,
    "titel" character varying(255),
    "reportNumber" character varying(255),
    "page" character varying(50),
    "dataOwner" smallint,
    "modifiedBy" character varying(50),
    "issn" character varying(50),
    "isbn" character varying(50),
    "journalName" character varying(100),
    "ownerInstitutionCode" character varying(25),
    "volume" smallint,
    "tempID" integer,
    "issue" integer,
    "referenceType" character varying(50)
);


--
-- Name: COLUMN "m_reference"."dataOwner"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."m_reference"."dataOwner" IS 'redundant field? does not belong in the reference table?';


--
-- Name: COLUMN "m_reference"."tempID"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."m_reference"."tempID" IS 'This field is only used for temporary IDs used to match refferences in relation to database bulk import';


--
-- Name: a_referanse_referanse_id_seq; Type: SEQUENCE; Schema: nofa; Owner: -
--

CREATE SEQUENCE "nofa"."a_referanse_referanse_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: a_referanse_referanse_id_seq; Type: SEQUENCE OWNED BY; Schema: nofa; Owner: -
--

ALTER SEQUENCE "nofa"."a_referanse_referanse_id_seq" OWNED BY "nofa"."m_reference"."referenceID";


--
-- Name: m_dataset; Type: TABLE; Schema: nofa; Owner: -
--

CREATE TABLE "nofa"."m_dataset" (
    "datasetID" character varying(255) NOT NULL,
    "datasetName" character varying(255),
    "institutionCode" character varying(50),
    "rightsHolder" character varying(255),
    "accessRights" character varying(255),
    "license" character varying(255),
    "informationWithheld" character varying(255),
    "dataGeneralizations" character varying(255),
    "bibliographicCitation" "text",
    "datasetComment" "text",
    "ownerInstitutionCode" character varying(25)
);


--
-- Name: TABLE "m_dataset"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON TABLE "nofa"."m_dataset" IS 'Table contains metadata about dataset. Ideally, the metadata should be available and registred on GBIF regardless of the status of accessRigths. If accessRigths are "private", then at least the metadata are available. The datasetID should then correspond to the identifier of the metadata (e.g. 78360224-5493-45fd-a9a0-c336557f09c3) and made resolvable by adding a prefix (e.g. http://www.gbif.org/dataset/78360224-5493-45fd-a9a0-c336557f09c3). Alternatively, the shortname used when registring on the IPT could be used, shortname could be generated also before registring on IPT and reused in that instance (e.g. lepidurus-arcticus-survey_northeast-greenland_2013 NB! No whitespaces or special characthers). This could also be made resolvable by adding a prefix (e.g. http://gbif.vm.ntnu.no/ipt/resource?r=lepidurus-arcticus-survey_northeast-greenland_2013).';


--
-- Name: COLUMN "m_dataset"."datasetID"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."m_dataset"."datasetID" IS 'If possible, reuse existing identifers (e.g. DOI as registred by GBIF if dataset alreaddy is published). For own data, use identifier that is globally unique and may be reused as alternative identifer when data is publised (UUID). Dwc definition: An identifier for the set of data. May be a global unique identifier or an identifier specific to a collection or institution. See http://rs.tdwg.org/dwc/terms/#datasetID';


--
-- Name: COLUMN "m_dataset"."datasetName"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."m_dataset"."datasetName" IS 'DwC definition: The name identifying the data set from which the record was derived. See http://rs.tdwg.org/dwc/terms/datasetName';


--
-- Name: COLUMN "m_dataset"."institutionCode"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."m_dataset"."institutionCode" IS 'DwC definition: The name (or acronym) in use by the institution having custody of the object(s) or information referred to in the record. http://rs.tdwg.org/dwc/terms/institutionCode. Example: NINA, NTNU-VM, UiT';


--
-- Name: COLUMN "m_dataset"."rightsHolder"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."m_dataset"."rightsHolder" IS 'DC definition: A person or organization owning or managing rights over the resource. See http://purl.org/dc/terms/rightsHolder. Comment: In almost all cases (for Norwegian data at least) the rightsHolder is an institution (https://lovdata.no/dokument/NL/lov/1961-05-12-2)';


--
-- Name: COLUMN "m_dataset"."accessRights"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."m_dataset"."accessRights" IS 'Indication of access status. Use either "open", "restricted" or "private" to indicate the level of privacy set to the data. "Open" indicates that the data is, or should be publicly awailable. In case of status "public", please use the field "licence" to indicate or set the appropriate licence to the data. In case of "restricted", pleace confer with the datasetComment field for instruction of use and contact person(s). In case of "private", the data should not be displayed or read by any service connected to NOFA. -i.e. use this the presence of "private" in this field to block from public views or maps.  DC definition: Information about who can access the resource or an indication of its security status. Access Rights may include information regarding access or restrictions based on privacy, security, or other policies. See http://purl.org/dc/terms/accessRights';


--
-- Name: COLUMN "m_dataset"."license"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."m_dataset"."license" IS 'The licence assigned to the data. In case of own primary data, pleace add this according to the DC definition and use either CC0 or CC-BY, or NLOD (confer with the data publishing policy of your instition). DC definition: A legal document giving official permission to do something with the resource. Examples: "http://creativecommons.org/publicdomain/zero/1.0/legalcode", "http://creativecommons.org/licenses/by/4.0/legalcode", "https://data.norge.no/nlod/no/1.0".';


--
-- Name: COLUMN "m_dataset"."informationWithheld"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."m_dataset"."informationWithheld" IS 'A pipe (|) sepparated list of fields (a.k.a column names) or Measurement and Fact entries (measurmentType values) that is witheld from public view (in case of access rigths public or restricted) and that not should be included in data-publication or any other electronic service going out from the database. This filed may be used to automatically restrict information flow from the database. Hence, be sure that the fields are correctly spelled and that there are no whitespaces between sepparator (|) and terms. DwC definintion: Additional information that exists, but that has not been shared in the given. See http://rs.tdwg.org/dwc/terms/informationWithheld record.';


--
-- Name: COLUMN "m_dataset"."dataGeneralizations"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."m_dataset"."dataGeneralizations" IS 'DwC definition: Actions taken to make the shared data less specific or complete than in its original form. Suggests that alternative data of higher quality may be available on request. Example: "Coordinates generalized from original GPS coordinates to the nearest half degree grid cell". See http://rs.tdwg.org/dwc/terms/informationWithheld';


--
-- Name: COLUMN "m_dataset"."bibliographicCitation"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."m_dataset"."bibliographicCitation" IS 'DwC definition: A bibliographic reference for the resource as a statement indicating how this record should be cited (attributed) when used. Recommended practice is to include sufficient bibliographic detail to identify the resource as unambiguously as possible. http://rs.tdwg.org/dwc/terms/#dcterms:bibliographicCitation';


--
-- Name: dwc_a_eventcore_emof; Type: VIEW; Schema: nofa; Owner: -
--

CREATE VIEW "nofa"."dwc_a_eventcore_emof" AS
 SELECT "concat"('urn:uuid:', "occurrence"."eventID") AS "eventID",
    "concat"('urn:uuid:', "occurrence"."occurrenceID") AS "occurrenceID",
    "concat"('populationTrend') AS "measurementType",
    "occurrence"."populationTrend" AS "measurementValue",
    "concat"('ordinal scale') AS "measurementUnit",
    "concat"('population trend from IUCN vocabulary, expanded with extinction and introduction as subclass of decrease and increase, respectively; http://www.iucnredlist.org/technical-documents/categories-and-criteria/2001-categories-criteria') AS "measurementMethod",
    "m_dataset"."datasetName"
   FROM (("nofa"."occurrence"
     JOIN "nofa"."event" ON (("event"."eventID" = "occurrence"."eventID")))
     JOIN "nofa"."m_dataset" ON ((("m_dataset"."datasetID")::"text" = ("event"."datasetID")::"text")))
  WHERE ("occurrence"."populationTrend" IS NOT NULL);


--
-- Name: dwc_a_eventcore_event; Type: VIEW; Schema: nofa; Owner: -
--

CREATE VIEW "nofa"."dwc_a_eventcore_event" AS
 SELECT "concat"('urn:uuid:', "event"."eventID") AS "eventID",
    "event"."parentEventID",
    "event"."fieldNumber",
    "m_dataset"."datasetName",
    "location"."waterBody",
    "location"."municipality",
    "location"."county",
    "concat"('NVE Vatn_lnr:', "location"."no_vatn_lnr", ' | NOFA locationID:', "location"."locationID") AS "locationID",
    "location"."decimalLongitude",
    "concat"('NINA') AS "institutionCode",
    "concat"('NINA_freswhater_fish') AS "collectionCode",
    "location"."decimalLatitude",
    "concat"('EPSG:4326') AS "geodeticDatum",
    "concat"('15') AS "coordinateUncertaintyInMeters",
    "concat"('Latitude/longitude coordinates represent ', "location"."locationType") AS "georeferenceRemarks",
    "concat"("event"."dateStart", '/', "event"."dateEnd") AS "eventDate",
    "date_part"('year'::"text", "event"."dateEnd") AS "year",
    "date_part"('month'::"text", "event"."dateEnd") AS "month",
    "date_part"('day'::"text", "event"."dateEnd") AS "day",
    "event"."samplingProtocol",
    "event"."samplingEffort",
    "event"."sampleSizeUnit",
    "event"."sampleSizeValue",
    "event"."modified",
    "location"."waterBodyID"
   FROM (("nofa"."event"
     JOIN "nofa"."m_dataset" ON ((("m_dataset"."datasetID")::"text" = ("event"."datasetID")::"text")))
     JOIN "nofa"."location" ON (("location"."locationID" = "event"."locationID")));


--
-- Name: l_taxon; Type: TABLE; Schema: nofa; Owner: -
--

CREATE TABLE "nofa"."l_taxon" (
    "speciesID" integer,
    "scientificName" character varying(50),
    "taxonID" integer NOT NULL,
    "scientificNameAuthorship" character varying(50),
    "vernacularName" character varying(50),
    "scientificNameSynonym" character varying(50),
    "vernacularName_NO" character varying(50),
    "vernacularName_SE" character varying(50),
    "vernacularName_FI" character varying(50),
    "kingdom" character varying(50),
    "phylum" character varying(50),
    "class" character varying(50),
    "order" character varying(50),
    "family" character varying(50),
    "genus" character varying(50),
    "subgenus" character varying(50),
    "taxonRank" character varying(50),
    "taxonRemarks" character varying(300),
    "key" integer,
    "parent" "text",
    "datasetKey" "text",
    "nameType" "text",
    "taxonomicStatus" "text",
    "nubKey" integer,
    "doesResolve" "text",
    "resolved_taxonID" "text",
    "publishedIn" "text"
);


--
-- Name: COLUMN "l_taxon"."speciesID"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."l_taxon"."speciesID" IS 'old taxonID, not used. Identical with the current ecotypeID. Delete';


--
-- Name: COLUMN "l_taxon"."scientificName"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."l_taxon"."scientificName" IS 'the canoncial name (binominal scientific name without author and year). ';


--
-- Name: COLUMN "l_taxon"."taxonID"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."l_taxon"."taxonID" IS 'Using as much as possible taxonIDs from Artsnavnebasen, however, some issues with the origin and stability of these. Care should be undertaken when intepreting these with a meaning outside the database. ';


--
-- Name: COLUMN "l_taxon"."scientificNameAuthorship"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."l_taxon"."scientificNameAuthorship" IS 'Autoresolved field. Do not manually edit. See http://rs.tdwg.org/dwc/terms/#scientificNameAuthorship';


--
-- Name: COLUMN "l_taxon"."vernacularName"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."l_taxon"."vernacularName" IS 'A common or vernacular name. Dwc idtentifier: http://rs.tdwg.org/dwc/terms/vernacularName';


--
-- Name: COLUMN "l_taxon"."scientificNameSynonym"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."l_taxon"."scientificNameSynonym" IS 'Manually edited on need to identify synonyms used in original data sources. See http://rs.tdwg.org/dwc/terms/scientificNameAuthorship';


--
-- Name: COLUMN "l_taxon"."vernacularName_NO"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."l_taxon"."vernacularName_NO" IS 'The commonly used Norwegian vernacular name. Manually edited field upon need. ';


--
-- Name: COLUMN "l_taxon"."vernacularName_SE"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."l_taxon"."vernacularName_SE" IS 'The commonly used Swedish vernacular name. Manually edited field upon need. ';


--
-- Name: COLUMN "l_taxon"."vernacularName_FI"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."l_taxon"."vernacularName_FI" IS 'The commonly used Finnish vernacular name. Manually edited field upon need. ';


--
-- Name: COLUMN "l_taxon"."kingdom"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."l_taxon"."kingdom" IS 'Autoresolved, should not be manually edited. The full scientific name of the kingdom in which the taxon is classified: http://rs.tdwg.org/dwc/terms/kingdom';


--
-- Name: COLUMN "l_taxon"."phylum"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."l_taxon"."phylum" IS 'Autoresolved, should not be manually edited. The full scientific name of the phylum in which the taxon is classified: http://rs.tdwg.org/dwc/terms/phylum';


--
-- Name: COLUMN "l_taxon"."class"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."l_taxon"."class" IS 'Autoresolved, should not be manually edited. The full scientific name of the class in which the taxon is classified: http://rs.tdwg.org/dwc/terms/class';


--
-- Name: COLUMN "l_taxon"."order"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."l_taxon"."order" IS 'Autoresolved, should not be manually edited. The full scientific name of the order in which the taxon is classified: http://rs.tdwg.org/dwc/terms/order';


--
-- Name: COLUMN "l_taxon"."family"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."l_taxon"."family" IS 'Autoresolved, should not be manually edited. The full scientific name of the family in which the taxon is classified: http://rs.tdwg.org/dwc/terms/family';


--
-- Name: COLUMN "l_taxon"."genus"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."l_taxon"."genus" IS 'Autoresolved, should not be manually edited. The full scientific name of the genus in which the taxon is classified: ';


--
-- Name: COLUMN "l_taxon"."subgenus"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."l_taxon"."subgenus" IS 'The full scientific name of the subgenus in which the taxon is classified. Values should include the genus to avoid homonym confusion: http://rs.tdwg.org/dwc/terms/subgenus';


--
-- Name: COLUMN "l_taxon"."taxonRank"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."l_taxon"."taxonRank" IS 'The taxonomic rank of the most specific name in the scientificName: http://rs.tdwg.org/dwc/terms/taxonRank';


--
-- Name: COLUMN "l_taxon"."taxonRemarks"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."l_taxon"."taxonRemarks" IS 'Remarks about the taxonomic classification: http://rs.tdwg.org/dwc/terms/taxonRemarks';


--
-- Name: COLUMN "l_taxon"."key"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."l_taxon"."key" IS 'Autorelolved, do not edit manually. The GBIF identifier for the taxon. http://www.gbif.org/species/»key» resolves to the GBIF portal page of the taxon';


--
-- Name: COLUMN "l_taxon"."datasetKey"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."l_taxon"."datasetKey" IS 'Autoresolve, do not edit manually. The GBIF key (UUID) of the dataset (checklist) used to resolve the taxon information. ';


--
-- Name: COLUMN "l_taxon"."nameType"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."l_taxon"."nameType" IS 'Redundant – all names presented here should be scientific names – delete';


--
-- Name: COLUMN "l_taxon"."taxonomicStatus"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."l_taxon"."taxonomicStatus" IS 'Autoresolved, do not edit manually. The status of the use of the scientificName as a label for a taxon. Requires taxonomic opinion to define the scope of a taxon. Rules of priority then are used to define the taxonomic status of the nomenclature contained in that scope, combined with the experts opinion. It must be linked to a specific taxonomic reference that defines the concept. Recommended best practice is to use a controlled vocabulary: http://rs.tdwg.org/dwc/terms/taxonomicStatus';


--
-- Name: COLUMN "l_taxon"."nubKey"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."l_taxon"."nubKey" IS 'Autoresolved, do not edit manually. The GBIF backbone taxonomy  identifier for the taxon. http://www.gbif.org/species/»nubKey» resolves to the GBIF taxonomic bacbone portal page of the taxon';


--
-- Name: COLUMN "l_taxon"."doesResolve"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."l_taxon"."doesResolve" IS 'Autoresolved, do not edit manually. TRUE if the taxon information resolves to a published GBIF taxonomy ';


--
-- Name: COLUMN "l_taxon"."resolved_taxonID"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."l_taxon"."resolved_taxonID" IS 'Autoresolved, do not edit manually. The taxonID as returned by the resolving service (GBIF API). ';


--
-- Name: COLUMN "l_taxon"."publishedIn"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."l_taxon"."publishedIn" IS 'Autoresolved, do not edit manually. A reference for the publication in which the scientificName was originally established under the rules of the associated nomenclaturalCode. Eqvivalent to the DwC namePublishedIn (http://rs.tdwg.org/dwc/terms/namePublishedIn)? ';


--
-- Name: dwc_a_eventcore_occurrence; Type: VIEW; Schema: nofa; Owner: -
--

CREATE VIEW "nofa"."dwc_a_eventcore_occurrence" AS
 SELECT "concat"('urn:uuid:', "occurrence"."eventID") AS "eventID",
    "concat"('urn:uuid:', "occurrence"."occurrenceID") AS "occurrenceID",
    "occurrence"."recordNumber",
    "concat"('HumanObservation') AS "basisOfRecord",
    "l_taxon"."scientificName",
    "l_taxon"."scientificNameAuthorship",
    "l_taxon"."vernacularName",
    "l_taxon"."kingdom",
    "l_taxon"."phylum",
    "l_taxon"."class",
    "l_taxon"."order",
    "l_taxon"."family",
    "l_taxon"."genus",
    "l_taxon"."taxonRank",
    "occurrence"."organismQuantity",
    "occurrence"."organismQuantityType",
    "occurrence"."individualCount",
    "occurrence"."occurrenceStatus",
    "occurrence"."establishmentMeans",
    "occurrence"."sex",
    "occurrence"."lifeStage",
    "occurrence"."reproductiveCondition",
    "event"."recordedBy",
    "m_dataset"."datasetName"
   FROM ((("nofa"."occurrence"
     JOIN "nofa"."l_taxon" ON (("l_taxon"."taxonID" = "occurrence"."taxonID")))
     JOIN "nofa"."event" ON (("event"."eventID" = "occurrence"."eventID")))
     JOIN "nofa"."m_dataset" ON ((("m_dataset"."datasetID")::"text" = ("event"."datasetID")::"text")));


--
-- Name: input_locations; Type: TABLE; Schema: nofa; Owner: -
--

CREATE TABLE "nofa"."input_locations" (
    "row.names" "text",
    "locationID_pike" "text",
    "decimalLatitude" double precision,
    "decimalLongitude" double precision
);


--
-- Name: l_basisOfRecord; Type: TABLE; Schema: nofa; Owner: -
--

CREATE TABLE "nofa"."l_basisOfRecord" (
    "basisOfRecord" character varying(50) NOT NULL,
    "description" character varying(255),
    "observasjonsType" character varying(50),
    "beskrivelse" character varying(255)
);


--
-- Name: l_ecotype; Type: TABLE; Schema: nofa; Owner: -
--

CREATE TABLE "nofa"."l_ecotype" (
    "ecotypeID" integer NOT NULL,
    "scientificName" character varying(50),
    "taxonID" integer,
    "scientificNameAuthorship" character varying(50),
    "vernacularName" character varying(50),
    "scientificNameSynonym" character varying(50),
    "vernacularName_NO" character varying(50),
    "vernacularName_SE" character varying(50),
    "vernacularName_FI" character varying(50),
    "kingdom" character varying(50),
    "phylum" character varying(50),
    "class" character varying(50),
    "order" character varying(50),
    "family" character varying(50),
    "genus" character varying(50),
    "subgenus" character varying(50),
    "taxonRank" character varying(50),
    "taxonRemarks" character varying(50)
);


--
-- Name: l_establishmentMeans; Type: TABLE; Schema: nofa; Owner: -
--

CREATE TABLE "nofa"."l_establishmentMeans" (
    "opprinnelse" character varying(50),
    "beskrivelse" character varying(500),
    "establishmentMeans" character varying(50) NOT NULL,
    "description" character varying(500)
);


--
-- Name: l_eventDateQualifier; Type: TABLE; Schema: nofa; Owner: -
--

CREATE TABLE "nofa"."l_eventDateQualifier" (
    "eventDateQualifier" character varying(50) NOT NULL,
    "description" character varying(500)
);


--
-- Name: l_lifeStage; Type: TABLE; Schema: nofa; Owner: -
--

CREATE TABLE "nofa"."l_lifeStage" (
    "lifeStage" character(15) NOT NULL
);


--
-- Name: TABLE "l_lifeStage"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON TABLE "nofa"."l_lifeStage" IS 'Namespace partly from http://rs.gbif.org/vocabulary/gbif/life_stage.xml,
with the use of some alternative terms';


--
-- Name: l_locationType; Type: TABLE; Schema: nofa; Owner: -
--

CREATE TABLE "nofa"."l_locationType" (
    "locationType" character varying(50) NOT NULL,
    "description" character varying(255),
    "lokalitetsType" character varying(50),
    "beskrivelse" character varying(255)
);


--
-- Name: TABLE "l_locationType"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON TABLE "nofa"."l_locationType" IS 'List of classifiers for the type of location';


--
-- Name: l_mof_occurrence; Type: TABLE; Schema: nofa; Owner: -
--

CREATE TABLE "nofa"."l_mof_occurrence" (
    "measurementType" character(255) NOT NULL,
    "measurementMethod" character(255),
    "measurementUnit" character(255),
    "measurementMethodRemarks" character(255)
);


--
-- Name: l_occurrenceStatus; Type: TABLE; Schema: nofa; Owner: -
--

CREATE TABLE "nofa"."l_occurrenceStatus" (
    "occurrenceStatus" character varying(50) NOT NULL,
    "description" character varying(500),
    "stockStatus" character varying(250)
);


--
-- Name: l_organismQuantityType; Type: TABLE; Schema: nofa; Owner: -
--

CREATE TABLE "nofa"."l_organismQuantityType" (
    "organismQuantityType" character varying(50) NOT NULL,
    "comment" character varying(500)
);


--
-- Name: TABLE "l_organismQuantityType"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON TABLE "nofa"."l_organismQuantityType" IS 'controlled vocabulary for organism quantity type from http://rs.gbif.org/vocabulary/gbif/quantity_type_2015-07-10.xml';


--
-- Name: COLUMN "l_organismQuantityType"."organismQuantityType"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."l_organismQuantityType"."organismQuantityType" IS 'controlled vocabulary for organism quantity type from http://rs.gbif.org/vocabulary/gbif/quantity_type_2015-07-10.xml';


--
-- Name: l_populationTrend; Type: TABLE; Schema: nofa; Owner: -
--

CREATE TABLE "nofa"."l_populationTrend" (
    "populationTrendID" integer NOT NULL,
    "populationTrend" character varying(50),
    "description" character varying(500),
    "stockChange" character varying(250)
);


--
-- Name: l_populationTrend_populationTrendID_seq; Type: SEQUENCE; Schema: nofa; Owner: -
--

CREATE SEQUENCE "nofa"."l_populationTrend_populationTrendID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: l_populationTrend_populationTrendID_seq; Type: SEQUENCE OWNED BY; Schema: nofa; Owner: -
--

ALTER SEQUENCE "nofa"."l_populationTrend_populationTrendID_seq" OWNED BY "nofa"."l_populationTrend"."populationTrendID";


--
-- Name: l_referenceType; Type: TABLE; Schema: nofa; Owner: -
--

CREATE TABLE "nofa"."l_referenceType" (
    "referanseTypeID" smallint NOT NULL,
    "referansetype" character varying(50),
    "beskrivelse" character varying(255),
    "referenceType" character varying(50),
    "description" character varying(255)
);


--
-- Name: l_reliability; Type: TABLE; Schema: nofa; Owner: -
--

CREATE TABLE "nofa"."l_reliability" (
    "reliability" character varying(50) NOT NULL,
    "description" character varying(500)
);


--
-- Name: l_reproductiveCondition; Type: TABLE; Schema: nofa; Owner: -
--

CREATE TABLE "nofa"."l_reproductiveCondition" (
    "reproductiveCondition" character(50) NOT NULL
);


--
-- Name: TABLE "l_reproductiveCondition"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON TABLE "nofa"."l_reproductiveCondition" IS 'Namespace partly from http://terms.tdwg.org/wiki/dwc:reproductiveCondition';


--
-- Name: l_rettighetshaver; Type: TABLE; Schema: nofa; Owner: -
--

CREATE TABLE "nofa"."l_rettighetshaver" (
    "rettighetshaver_id" smallint NOT NULL,
    "navn" character varying(50),
    "eksklusive_data" character varying(50),
    "beskrivelse" character varying(255)
);


--
-- Name: TABLE "l_rettighetshaver"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON TABLE "nofa"."l_rettighetshaver" IS 'Should be droped when column dataOwner has been removed from m_reference';


--
-- Name: l_sampleSizeUnit; Type: TABLE; Schema: nofa; Owner: -
--

CREATE TABLE "nofa"."l_sampleSizeUnit" (
    "sampleSizeUnit" character varying(30) NOT NULL,
    "description" "text"
);


--
-- Name: l_samplingProtocol; Type: TABLE; Schema: nofa; Owner: -
--

CREATE TABLE "nofa"."l_samplingProtocol" (
    "metode" character varying(50),
    "beskrivelse" character varying(255),
    "samplingProtocol" character varying(255) NOT NULL,
    "description" character varying(500),
    "typeOfSamplingGear" character varying(255),
    "samplingType" character varying(255),
    "typeOfGilnet" character varying(255),
    "nameOfGillnetSeries" character varying(255),
    "bottomOrFloat" character varying(255),
    "samplingProtocolStandard" character varying(255),
    "samplingProtocolHabitat" character varying(255),
    "SamplingSpot" character varying(255),
    "numberOfNetsInSerie" integer,
    "GillNetArea" double precision,
    "SamplingHabitat" character varying(255),
    "GillNetLength_m" double precision,
    "GillNetDepth_m" double precision,
    "GillNetMeshSize_mm" character varying(255),
    "NumberOfGillnetMeshSizes" integer,
    "depth" character varying(255),
    "habitat" character varying(255),
    "vertabrimSamplingProtocol" character varying(255),
    "samplingProtocolRemarks" "text",
    "Standards" character varying(255),
    "URL" "text",
    "Reference" character varying(255)
);


--
-- Name: l_sex; Type: TABLE; Schema: nofa; Owner: -
--

CREATE TABLE "nofa"."l_sex" (
    "sex" character(15) NOT NULL
);


--
-- Name: TABLE "l_sex"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON TABLE "nofa"."l_sex" IS 'Namespace from http://rs.gbif.org/vocabulary/gbif/sex.xml';


--
-- Name: l_spawningCondition; Type: TABLE; Schema: nofa; Owner: -
--

CREATE TABLE "nofa"."l_spawningCondition" (
    "gyteforhold" character varying(50),
    "beskrivelse" character varying(255),
    "spawningCondition" character varying(50) NOT NULL,
    "description" character varying(255)
);


--
-- Name: l_spawningLocation; Type: TABLE; Schema: nofa; Owner: -
--

CREATE TABLE "nofa"."l_spawningLocation" (
    "gytested" character varying(50),
    "beskrivelse" character varying(255),
    "spawningLocation" character varying(50) NOT NULL,
    "description" character varying(255)
);


--
-- Name: l_taxon_historicDistribution; Type: TABLE; Schema: nofa; Owner: -
--

CREATE TABLE "nofa"."l_taxon_historicDistribution" (
    "localityID" integer NOT NULL,
    "taxonID" integer,
    "dateStart" "date",
    "dateEnd" "date",
    "referenceID" integer,
    "occurrence" smallint,
    "label" character varying(50),
    "geom" "public"."geometry"(Polygon,25833)
);


--
-- Name: l_taxon_historicDistribution_localityID_seq; Type: SEQUENCE; Schema: nofa; Owner: -
--

CREATE SEQUENCE "nofa"."l_taxon_historicDistribution_localityID_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: l_taxon_historicDistribution_localityID_seq; Type: SEQUENCE OWNED BY; Schema: nofa; Owner: -
--

ALTER SEQUENCE "nofa"."l_taxon_historicDistribution_localityID_seq" OWNED BY "nofa"."l_taxon_historicDistribution"."localityID";


--
-- Name: lakeID_seq; Type: SEQUENCE; Schema: nofa; Owner: -
--

CREATE SEQUENCE "nofa"."lakeID_seq"
    START WITH 986220
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: location_id_seq; Type: SEQUENCE; Schema: nofa; Owner: -
--

CREATE SEQUENCE "nofa"."location_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: location_id_seq; Type: SEQUENCE OWNED BY; Schema: nofa; Owner: -
--

ALTER SEQUENCE "nofa"."location_id_seq" OWNED BY "nofa"."waterBody"."id";


--
-- Name: mapping_info; Type: TABLE; Schema: nofa; Owner: -
--

CREATE TABLE "nofa"."mapping_info" (
    "mapping_infoID" "uuid" DEFAULT "public"."uuid_generate_v4"() NOT NULL,
    "datasetID" "uuid" DEFAULT 'ea9e11e3-0822-49b1-bc7c-b9236a004e9f'::"uuid" NOT NULL,
    "destination_table" character varying(255),
    "destination_term" character varying(255),
    "source_term" character varying(255),
    "translation_refference" character varying(255)
);


--
-- Name: TABLE "mapping_info"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON TABLE "nofa"."mapping_info" IS ' - A list of information log describing the mapping process for each imported dataset.';


--
-- Name: COLUMN "mapping_info"."mapping_infoID"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."mapping_info"."mapping_infoID" IS ' - A unique global identifier for a mapping information log.';


--
-- Name: COLUMN "mapping_info"."datasetID"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."mapping_info"."datasetID" IS ' - The datasetID, foreign key to the datasetID column in the m_datset table';


--
-- Name: COLUMN "mapping_info"."destination_table"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."mapping_info"."destination_table" IS ' - The databese table for which the values are put into ';


--
-- Name: COLUMN "mapping_info"."destination_term"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."mapping_info"."destination_term" IS ' - the databasae term for which the values are populated into';


--
-- Name: COLUMN "mapping_info"."source_term"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."mapping_info"."source_term" IS ' - the orignal term used in the orginal data. Use pipe (|) sepparted list if the values orginates from multiple columns or sources.';


--
-- Name: COLUMN "mapping_info"."translation_refference"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON COLUMN "nofa"."mapping_info"."translation_refference" IS ' - A verbatim desciption of the translation process, beeing either directly pasted from the data source or generated/calculated during the mapping procedure.';


--
-- Name: mof_occurrence; Type: TABLE; Schema: nofa; Owner: -
--

CREATE TABLE "nofa"."mof_occurrence" (
    "measurementID" character varying(255) NOT NULL,
    "occurrenceID_serial" character varying(255),
    "measurementMethod" character varying(255),
    "measurementType" character varying(255),
    "measurementValue" character varying(255),
    "measurementAccuracy" character varying(255),
    "measurementUnit" character varying(255),
    "measurementDeterminedBy" character varying(255),
    "measurementDeterminedDate" character varying(255),
    "measurementRemarks" character varying(255),
    "occurrenceID" "uuid"
);


--
-- Name: TABLE "mof_occurrence"; Type: COMMENT; Schema: nofa; Owner: -
--

COMMENT ON TABLE "nofa"."mof_occurrence" IS 'Measurments Or Facts on records in the occurrence table. Contains all additional measurement typically not related to organismQuantiy (i.e. not existing DwC terms). Uses controlled vocabulary from the l_mof_occurrence table. One drawback with this method is that all measurements are stored as text. Hence, needs to be converted back to numbers upon use, and there is no way to control values exept for during input of data. Alternative is to construct two sepparate tables, one for caracther and one for numbers';


--
-- Name: number_populations_5000m_pike; Type: MATERIALIZED VIEW; Schema: nofa; Owner: -
--

CREATE MATERIALIZED VIEW "nofa"."number_populations_5000m_pike" AS
 SELECT "al"."id" AS "waterBodyID",
    "count"("ol"."geom") AS "count"
   FROM "nofa"."lake" "al",
    ( SELECT "lake"."geom"
           FROM "nofa"."lake"
          WHERE ("lake"."id" IN ( SELECT "get_last_occurrence_status"."waterBodyID"
                   FROM "nofa"."get_last_occurrence_status"("taxonID" => 26181) "get_last_occurrence_status"("wrid", "waterBodyID", "locationID", "occurrenceStatus", "reliability", "dateLast")))) "ol"
  WHERE "public"."st_dwithin"("al"."geom", "ol"."geom", (5000)::double precision)
  GROUP BY "al"."id"
  WITH NO DATA;


--
-- Name: otherwaterbody; Type: TABLE; Schema: nofa; Owner: -
--

CREATE TABLE "nofa"."otherwaterbody" (
    "elevation" double precision
)
INHERITS ("nofa"."waterBody");


--
-- Name: samplingTaxaRange_id_seq; Type: SEQUENCE; Schema: nofa; Owner: -
--

CREATE SEQUENCE "nofa"."samplingTaxaRange_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: samplingTaxaRange; Type: TABLE; Schema: nofa; Owner: -
--

CREATE TABLE "nofa"."samplingTaxaRange" (
    "id" integer DEFAULT "nextval"('"nofa"."samplingTaxaRange_id_seq"'::"regclass") NOT NULL,
    "taxonID" integer NOT NULL,
    "eventID" "uuid" NOT NULL
);


--
-- Name: simulation_out_test; Type: TABLE; Schema: nofa; Owner: -
--

CREATE TABLE "nofa"."simulation_out_test" (
    "waterBodyID" double precision,
    "long" double precision,
    "lat" double precision,
    "n_intro" double precision,
    "p_intro" double precision
);


--
-- Name: stream; Type: TABLE; Schema: nofa; Owner: -
--

CREATE TABLE "nofa"."stream" (
    "lengthM" double precision,
    "elevationMinM" double precision,
    "elevationMaxM" double precision,
    "geom" "public"."geometry"(LineString,25833)
)
INHERITS ("nofa"."waterBody");


--
-- Name: temp_m_dataset_data_import; Type: TABLE; Schema: nofa; Owner: -
--

CREATE TABLE "nofa"."temp_m_dataset_data_import" (
    "datasetID" "text",
    "datasetName" "text",
    "institutionCode" "text",
    "rightsHolder" "text",
    "accessRights" "text",
    "license" "text",
    "informationWithheld" "text",
    "dataGeneralizations" "text",
    "bibliographicCitation" "text",
    "datasetComment" "text",
    "ownerInstitutionCode" "text"
);


--
-- Name: test_view_sam; Type: VIEW; Schema: nofa; Owner: -
--

CREATE VIEW "nofa"."test_view_sam" AS
 SELECT "row_number"() OVER () AS "OID",
    "l_taxon"."scientificName",
    "l_taxon"."vernacularName",
    "sum"("occurrence"."individualCount") AS "individualCount",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."occurrenceStatus"), '|'::"text") AS "occurrenceStatus",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."establishmentMeans"), '|'::"text") AS "establishmentMeans",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."recordNumber"), '|'::"text") AS "recordNumber",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."populationTrend"), '|'::"text") AS "populationTrend",
    "array_to_string"("array_agg"(DISTINCT "event"."datasetID"), '|'::"text") AS "datasetID",
    "string_agg"(("occurrence"."occurrenceID")::"text", ' | '::"text") AS "occurrenceID",
    "event"."dateEnd",
    "location"."geom",
    "location"."waterBodyID",
    "location"."eb_waterregionID"
   FROM ((("nofa"."occurrence"
     JOIN "nofa"."event" ON (("occurrence"."eventID" = "event"."eventID")))
     JOIN "nofa"."location" ON (("event"."locationID" = "location"."locationID")))
     JOIN "nofa"."l_taxon" ON (("occurrence"."taxonID" = "l_taxon"."taxonID")))
  GROUP BY "event"."dateEnd", "event"."locationID", "l_taxon"."scientificName", "l_taxon"."vernacularName", "event"."datasetID", "location"."geom", "event"."fieldNumber", "location"."waterBodyID", "location"."eb_waterregionID";


--
-- Name: test_view_swedenperch; Type: VIEW; Schema: nofa; Owner: -
--

CREATE VIEW "nofa"."test_view_swedenperch" AS
 SELECT "row_number"() OVER () AS "OID",
    "l_taxon"."scientificName",
    "l_taxon"."vernacularName",
    "sum"("occurrence"."individualCount") AS "individualCount",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."occurrenceStatus"), '|'::"text") AS "occurrenceStatus",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."establishmentMeans"), '|'::"text") AS "establishmentMeans",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."recordNumber"), '|'::"text") AS "recordNumber",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."populationTrend"), '|'::"text") AS "populationTrend",
    "array_to_string"("array_agg"(DISTINCT "event"."datasetID"), '|'::"text") AS "datasetID",
    "string_agg"(("occurrence"."occurrenceID")::"text", ' | '::"text") AS "occurrenceID",
    "event"."dateEnd",
    "location"."geom",
    "location"."waterBodyID",
    "location"."eb_waterregionID",
    "location"."georeferenceSources"
   FROM ((("nofa"."occurrence"
     JOIN "nofa"."event" ON (("occurrence"."eventID" = "event"."eventID")))
     JOIN "nofa"."location" ON (("event"."locationID" = "location"."locationID")))
     JOIN "nofa"."l_taxon" ON (("occurrence"."taxonID" = "l_taxon"."taxonID")))
  WHERE (("location"."georeferenceSources" ~~ '%Sweden%'::"text") AND (("l_taxon"."vernacularName")::"text" = 'Perch'::"text"))
  GROUP BY "event"."dateEnd", "event"."locationID", "l_taxon"."scientificName", "l_taxon"."vernacularName", "event"."datasetID", "location"."geom", "event"."fieldNumber", "location"."waterBodyID", "location"."eb_waterregionID", "location"."georeferenceSources";


--
-- Name: view_aggregated_occurrence; Type: VIEW; Schema: nofa; Owner: -
--

CREATE VIEW "nofa"."view_aggregated_occurrence" AS
 SELECT "row_number"() OVER () AS "OID",
    "l_taxon"."scientificName",
    "l_taxon"."vernacularName",
    "l_taxon"."vernacularName_NO",
    "sum"("occurrence"."individualCount") AS "individualCount",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."occurrenceStatus"), '|'::"text") AS "occurrenceStatus",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."establishmentMeans"), '|'::"text") AS "establishmentMeans",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."recordNumber"), '|'::"text") AS "recordNumber",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."populationTrend"), '|'::"text") AS "populationTrend",
    "array_to_string"("array_agg"(DISTINCT "event"."datasetID"), '|'::"text") AS "datasetID",
    "string_agg"(("occurrence"."occurrenceID")::"text", ' | '::"text") AS "occurrenceID",
    "event"."dateEnd",
    "location"."decimalLatitude",
    "location"."decimalLongitude",
    "location"."municipality",
    "location"."county",
    "location"."geom",
    "location"."waterBodyID"
   FROM ((("nofa"."occurrence"
     JOIN "nofa"."event" ON (("occurrence"."eventID" = "event"."eventID")))
     JOIN "nofa"."location" ON (("event"."locationID" = "location"."locationID")))
     JOIN "nofa"."l_taxon" ON (("occurrence"."taxonID" = "l_taxon"."taxonID")))
  GROUP BY "event"."dateEnd", "event"."locationID", "l_taxon"."scientificName", "l_taxon"."vernacularName", "l_taxon"."vernacularName_NO", "event"."datasetID", "location"."decimalLatitude", "location"."decimalLongitude", "location"."municipality", "event"."fieldNumber", "location"."county", "location"."geom", "location"."waterBodyID";


--
-- Name: view_introductions; Type: VIEW; Schema: nofa; Owner: -
--

CREATE VIEW "nofa"."view_introductions" AS
 SELECT "occurrence"."eventID",
    "occurrence"."occurrenceID",
    "occurrence"."recordNumber",
    "l_taxon"."scientificName",
    "l_taxon"."vernacularName",
    "l_taxon"."taxonRank",
    "occurrence"."occurrenceStatus",
    "occurrence"."establishmentMeans",
    "occurrence"."populationTrend",
    "event"."recordedBy",
    "event"."dateStart",
    "event"."dateEnd",
    "event"."eventDateQualifier",
    "m_dataset"."datasetName",
    "location"."waterBody",
    "location"."decimalLongitude",
    "location"."decimalLatitude",
    "location"."countryCode",
    "location"."county",
    "location"."municipality",
    "location"."waterBodyID"
   FROM (((("nofa"."occurrence"
     JOIN "nofa"."l_taxon" ON (("l_taxon"."taxonID" = "occurrence"."taxonID")))
     JOIN "nofa"."event" ON (("event"."eventID" = "occurrence"."eventID")))
     JOIN "nofa"."location" ON (("location"."locationID" = "event"."locationID")))
     JOIN "nofa"."m_dataset" ON ((("m_dataset"."datasetID")::"text" = ("event"."datasetID")::"text")))
  WHERE (("occurrence"."establishmentMeans")::"text" = 'introduced'::"text");


--
-- Name: view_lake_environment; Type: VIEW; Schema: nofa; Owner: -
--

CREATE VIEW "nofa"."view_lake_environment" AS
 SELECT "lake"."id" AS "waterBodyID",
    "array_to_string"("lake"."nationalWaterBodyID", ','::"text") AS "nationalWaterBodyID",
    "array_to_string"("lake"."municipalityIDs", ','::"text") AS "municipalityIDs",
    "lake"."county",
    "array_to_string"("lake"."countryCode", ','::"text") AS "countryCode",
    "public"."st_x"("lake"."centroid") AS "utm_x",
    "public"."st_y"("lake"."centroid") AS "utm_y",
    "lake"."elevation" AS "minimumElevationInMeters",
    "lake"."elevation" AS "maximumElevationInMeters",
        CASE
            WHEN ("lake"."area_km2" = (0)::double precision) THEN (0)::double precision
            ELSE "log"("lake"."area_km2")
        END AS "area_km2_log",
        CASE
            WHEN ("lake"."distance_to_road" = 0) THEN (0)::double precision
            ELSE "log"(("lake"."distance_to_road")::double precision)
        END AS "distance_to_road_log",
    ((("lake"."perimeter_m" / 1000))::double precision / ((2)::double precision * "sqrt"(("pi"() * "lake"."area_km2")))) AS "SCI",
    "lake"."perimeter_m",
    "lake"."elevation",
    "lb"."buffer_5000m_population_2006",
    "lb"."buffer_5000m_population_2011",
    "array_to_string"("lb"."buffer_5000m_lakes", ','::"text") AS "buffer_5000m_lakes",
    "lb"."buffer_5000m_lakes_n",
    "lake_EuroLST_BioClim"."eurolst_bio01",
    "lake_EuroLST_BioClim"."eurolst_bio02",
    "lake_EuroLST_BioClim"."eurolst_bio03",
    "lake_EuroLST_BioClim"."eurolst_bio05",
    "lake_EuroLST_BioClim"."eurolst_bio06",
    "lake_EuroLST_BioClim"."eurolst_bio07",
    "lake_EuroLST_BioClim"."eurolst_bio10",
    "lake_EuroLST_BioClim"."eurolst_bio11",
    "lake"."centroid"
   FROM (("nofa"."lake"
     JOIN "environmental"."lake_EuroLST_BioClim" ON (("lake"."id" = "lake_EuroLST_BioClim"."waterBodyID")))
     JOIN ( SELECT "lake_buffer"."id",
            "lake_buffer"."buffer_5000m_lakes",
            "lake_buffer"."buffer_5000m_lakes_n",
            "lake_buffer"."buffer_5000m_population_2006",
            "lake_buffer"."buffer_5000m_population_2011"
           FROM "environmental"."lake_buffer") "lb" ON (("lake"."id" = "lb"."id")));


--
-- Name: view_location_environment; Type: VIEW; Schema: nofa; Owner: -
--

CREATE VIEW "nofa"."view_location_environment" AS
 SELECT "row_number"() OVER () AS "OID",
    "location"."locationID",
    "location"."waterBody",
    "location"."no_vatn_lnr" AS "vatn_lnr",
    "location"."municipality",
    "location"."county",
    "location"."countryCode",
    "public"."st_x"("public"."st_centroid"("location"."geom")) AS "utm_x",
    "public"."st_y"("public"."st_centroid"("location"."geom")) AS "utm_y",
    "location"."waterBodyID",
    "location"."minimumElevationInMeters",
    "location"."maximumElevationInMeters",
        CASE
            WHEN ("lake"."area_km2" = (0)::double precision) THEN (0)::double precision
            ELSE "log"("lake"."area_km2")
        END AS "area_km2_log",
        CASE
            WHEN ("lake"."distance_to_road" = 0) THEN (0)::double precision
            ELSE "log"(("lake"."distance_to_road")::double precision)
        END AS "distance_to_road_log",
    ((("lake"."perimeter_m" / 1000))::double precision / ((2)::double precision * "sqrt"(("pi"() * "lake"."area_km2")))) AS "SCI",
    "lake"."perimeter_m",
    "lake"."elevation",
    "lb"."buffer_5000m_population_2006",
    "lb"."buffer_5000m_population_2011",
    "location_EuroLST_BioClim"."eurolst_bio01",
    "location_EuroLST_BioClim"."eurolst_bio02",
    "location_EuroLST_BioClim"."eurolst_bio03",
    "location_EuroLST_BioClim"."eurolst_bio05",
    "location_EuroLST_BioClim"."eurolst_bio06",
    "location_EuroLST_BioClim"."eurolst_bio07",
    "location_EuroLST_BioClim"."eurolst_bio10",
    "location_EuroLST_BioClim"."eurolst_bio11",
    "location"."geom"
   FROM ((("nofa"."location"
     JOIN "nofa"."lake" ON (("location"."waterBodyID" = "lake"."id")))
     JOIN "environmental"."location_EuroLST_BioClim" ON (("location"."locationID" = "location_EuroLST_BioClim"."locationID")))
     JOIN ( SELECT "lake_buffer"."id",
            "lake_buffer"."buffer_5000m_population_2006",
            "lake_buffer"."buffer_5000m_population_2011"
           FROM "environmental"."lake_buffer") "lb" ON (("location"."waterBodyID" = "lb"."id")));


--
-- Name: view_locations_raw; Type: VIEW; Schema: nofa; Owner: -
--

CREATE VIEW "nofa"."view_locations_raw" AS
 SELECT "row_number"() OVER () AS "GID",
    "array_to_string"("array_agg"(DISTINCT "l_taxon"."scientificName"), '|'::"text") AS "scientificName",
    "array_to_string"("array_agg"(DISTINCT "l_taxon"."vernacularName"), '|'::"text") AS "vernacularName",
    "array_to_string"("array_agg"(DISTINCT "l_taxon"."vernacularName_NO"), '|'::"text") AS "vernacularName_NO",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."occurrenceStatus"), '|'::"text") AS "occurrenceStatus",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."establishmentMeans"), '|'::"text") AS "establishmentMeans",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."recordNumber"), '|'::"text") AS "recordNumber",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."populationTrend"), '|'::"text") AS "populationTrend",
    "array_to_string"("array_agg"(DISTINCT "event"."dateEnd"), '|'::"text") AS "dateEnd",
    "array_to_string"("array_agg"(DISTINCT "event"."eventID"), '|'::"text") AS "eventID",
    "array_to_string"("array_agg"("date_part"('year'::"text", "event"."dateEnd")), '|'::"text") AS "year",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."occurrenceID"), '|'::"text") AS "occurrenceID",
    "location"."decimalLatitude",
    "location"."decimalLongitude",
    "location"."waterBody",
    "location"."locationID",
    "location"."no_vatn_lnr",
    "location"."municipality",
    "location"."county",
    "location"."countryCode",
    "location"."georeferenceRemarks",
    "array_to_string"("array_agg"(DISTINCT "event"."datasetID"), '|'::"text") AS "datasetID",
    "array_to_string"("array_agg"(DISTINCT "m_dataset"."datasetName"), '|'::"text") AS "datasetName",
    "array_to_string"("array_agg"(DISTINCT "m_dataset"."rightsHolder"), '|'::"text") AS "rightsHolder",
    "array_to_string"("array_agg"(DISTINCT "m_dataset"."accessRights"), '|'::"text") AS "accessRights",
    "array_to_string"("array_agg"(DISTINCT "m_dataset"."bibliographicCitation"), '|'::"text") AS "bibliographicCitation",
    "location"."geom",
    "location"."locationType",
    "location"."waterBodyID",
    "lake"."area_km2",
    "lake"."elevation",
    "lake"."bathymetryAvailable",
    "lake"."perimeter_m",
    "lake"."distance_to_road"
   FROM ((((("nofa"."location"
     JOIN "nofa"."event" ON (("location"."locationID" = "event"."locationID")))
     JOIN "nofa"."occurrence" ON (("event"."eventID" = "occurrence"."eventID")))
     JOIN "nofa"."m_dataset" ON ((("event"."datasetID")::"text" = ("m_dataset"."datasetID")::"text")))
     JOIN "nofa"."lake" ON (("location"."waterBodyID" = "lake"."id")))
     JOIN "nofa"."l_taxon" ON (("occurrence"."taxonID" = "l_taxon"."taxonID")))
  GROUP BY "location"."decimalLatitude", "location"."decimalLongitude", "location"."municipality", "location"."county", "location"."countryCode", "location"."waterBody", "location"."locationID", "location"."no_vatn_lnr", "location"."geom", "location"."locationType", "location"."georeferenceRemarks", "location"."waterBodyID", "lake"."area_km2", "lake"."elevation", "lake"."bathymetryAvailable", "lake"."perimeter_m", "lake"."distance_to_road";


--
-- Name: view_occurrence_all; Type: VIEW; Schema: nofa; Owner: -
--

CREATE VIEW "nofa"."view_occurrence_all" AS
 SELECT "row_number"() OVER () AS "GID",
    "l_taxon"."scientificName",
    "l_taxon"."vernacularName",
    "l_taxon"."vernacularName_NO",
    "sum"("occurrence"."individualCount") AS "individualCount",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."occurrenceStatus"), '|'::"text") AS "occurrenceStatus",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."establishmentMeans"), '|'::"text") AS "establishmentMeans",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."recordNumber"), '|'::"text") AS "recordNumber",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."populationTrend"), '|'::"text") AS "populationTrend",
    "array_to_string"("array_agg"(DISTINCT "event"."dateEnd"), '|'::"text") AS "eventDate",
    "string_agg"(("occurrence"."occurrenceID")::"text", ' | '::"text") AS "occurrenceID",
    "location"."decimalLatitude",
    "location"."decimalLongitude",
    "location"."waterBody",
    "location"."locationID",
    "location"."no_vatn_lnr",
    "location"."municipality",
    "location"."county",
    "array_to_string"("array_agg"(DISTINCT "event"."datasetID"), '|'::"text") AS "datasetID",
    "array_to_string"("array_agg"(DISTINCT "m_dataset"."datasetName"), '|'::"text") AS "datasetName",
    "array_to_string"("array_agg"(DISTINCT "m_dataset"."rightsHolder"), '|'::"text") AS "rightsHolder",
    "array_to_string"("array_agg"(DISTINCT "m_dataset"."accessRights"), '|'::"text") AS "accessRights",
    "array_to_string"("array_agg"(DISTINCT "m_dataset"."bibliographicCitation"), '|'::"text") AS "bibliographicCitation",
    "location"."geom",
    "location"."waterBodyID"
   FROM (((("nofa"."occurrence"
     JOIN "nofa"."event" ON (("occurrence"."eventID" = "event"."eventID")))
     JOIN "nofa"."location" ON (("event"."locationID" = "location"."locationID")))
     JOIN "nofa"."m_dataset" ON ((("event"."datasetID")::"text" = ("m_dataset"."datasetID")::"text")))
     JOIN "nofa"."l_taxon" ON (("occurrence"."taxonID" = "l_taxon"."taxonID")))
  GROUP BY "event"."locationID", "l_taxon"."scientificName", "l_taxon"."vernacularName", "l_taxon"."vernacularName_NO", "event"."datasetID", "location"."decimalLatitude", "location"."decimalLongitude", "location"."municipality", "location"."county", "location"."waterBody", "location"."locationID", "location"."no_vatn_lnr", "location"."geom", "location"."waterBodyID";


--
-- Name: view_occurrence_by_event; Type: VIEW; Schema: nofa; Owner: -
--

CREATE VIEW "nofa"."view_occurrence_by_event" AS
SELECT
    NULL::bigint AS "OID",
    NULL::character varying(50) AS "scientificName",
    NULL::character varying(50) AS "vernacularName",
    NULL::character varying(50) AS "vernacularName_NO",
    NULL::bigint AS "individualCount",
    NULL::"text" AS "occurrenceStatus",
    NULL::"text" AS "establishmentMeans",
    NULL::"text" AS "recordNumber",
    NULL::"text" AS "populationTrend",
    NULL::"text" AS "occurrenceID",
    NULL::"date" AS "dateEnd",
    NULL::double precision AS "year",
    NULL::"uuid" AS "eventID",
    NULL::integer[] AS "samplingTaxaRange",
    NULL::character varying(100) AS "samplingProtocol",
    NULL::character varying(50) AS "recordedBy",
    NULL::character varying(50) AS "reliability",
    NULL::"uuid" AS "locationID",
    NULL::double precision AS "decimalLatitude",
    NULL::double precision AS "decimalLongitude",
    NULL::character varying(50) AS "waterBody",
    NULL::integer AS "vatn_lnr",
    NULL::character varying(25) AS "municipality",
    NULL::character varying(25) AS "county",
    NULL::character(2) AS "countryCode",
    NULL::double precision AS "utm_x",
    NULL::double precision AS "utm_y",
    NULL::integer AS "waterBodyID",
    NULL::double precision AS "minimumElevationInMeters",
    NULL::double precision AS "maximumElevationInMeters",
    NULL::character varying(255) AS "datasetName",
    NULL::character varying(255) AS "rightsHolder",
    NULL::character varying(255) AS "datasetID",
    NULL::"text" AS "bibliographicCitation",
    NULL::double precision AS "area_km2",
    NULL::integer AS "distance_to_road",
    NULL::integer AS "perimeter_m",
    NULL::double precision AS "elevation",
    NULL::double precision AS "buffer_5000m_population_2006",
    NULL::double precision AS "buffer_5000m_population_2011",
    NULL::double precision AS "eurolst_bio01",
    NULL::double precision AS "eurolst_bio02",
    NULL::double precision AS "eurolst_bio03",
    NULL::double precision AS "eurolst_bio05",
    NULL::double precision AS "eurolst_bio06",
    NULL::double precision AS "eurolst_bio07",
    NULL::double precision AS "eurolst_bio10",
    NULL::double precision AS "eurolst_bio11",
    NULL::"public"."geometry"(MultiPoint,25833) AS "geom";


--
-- Name: view_occurrence_by_event_all; Type: VIEW; Schema: nofa; Owner: -
--

CREATE VIEW "nofa"."view_occurrence_by_event_all" AS
 SELECT "row_number"() OVER () AS "GID",
    "l_taxon"."scientificName",
    "l_taxon"."vernacularName",
    "l_taxon"."vernacularName_NO",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."occurrenceStatus"), '|'::"text") AS "occurrenceStatus",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."establishmentMeans"), '|'::"text") AS "establishmentMeans",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."recordNumber"), '|'::"text") AS "recordNumber",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."populationTrend"), '|'::"text") AS "populationTrend",
    "event"."dateEnd" AS "eventDate",
    "event"."eventID",
    "date_part"('year'::"text", "event"."dateEnd") AS "year",
    "string_agg"(("occurrence"."occurrenceID")::"text", ' | '::"text") AS "occurrenceID",
    "location"."decimalLatitude",
    "location"."decimalLongitude",
    "location"."waterBody",
    "location"."locationID",
    "location"."no_vatn_lnr",
    "location"."municipality",
    "location"."county",
    "location"."countryCode",
    "array_to_string"("array_agg"(DISTINCT "event"."datasetID"), '|'::"text") AS "datasetID",
    "array_to_string"("array_agg"(DISTINCT "m_dataset"."datasetName"), '|'::"text") AS "datasetName",
    "array_to_string"("array_agg"(DISTINCT "m_dataset"."rightsHolder"), '|'::"text") AS "rightsHolder",
    "array_to_string"("array_agg"(DISTINCT "m_dataset"."accessRights"), '|'::"text") AS "accessRights",
    "array_to_string"("array_agg"(DISTINCT "m_dataset"."bibliographicCitation"), '|'::"text") AS "bibliographicCitation",
    "location"."geom",
    "location"."waterBodyID"
   FROM (((("nofa"."occurrence"
     JOIN "nofa"."event" ON (("occurrence"."eventID" = "event"."eventID")))
     JOIN "nofa"."location" ON (("event"."locationID" = "location"."locationID")))
     JOIN "nofa"."m_dataset" ON ((("event"."datasetID")::"text" = ("m_dataset"."datasetID")::"text")))
     JOIN "nofa"."l_taxon" ON (("occurrence"."taxonID" = "l_taxon"."taxonID")))
  GROUP BY "event"."eventID", "event"."dateEnd", "event"."locationID", "l_taxon"."scientificName", "l_taxon"."vernacularName", "l_taxon"."vernacularName_NO", "event"."datasetID", "location"."decimalLatitude", "location"."decimalLongitude", "location"."municipality", "location"."county", "location"."countryCode", "location"."waterBody", "location"."locationID", "location"."no_vatn_lnr", "location"."geom", "location"."waterBodyID";


--
-- Name: view_occurrence_by_event_all_samplingprotocol; Type: VIEW; Schema: nofa; Owner: -
--

CREATE VIEW "nofa"."view_occurrence_by_event_all_samplingprotocol" AS
SELECT
    NULL::bigint AS "GID",
    NULL::character varying(50) AS "scientificName",
    NULL::character varying(50) AS "vernacularName",
    NULL::character varying(50) AS "vernacularName_NO",
    NULL::"text" AS "occurrenceStatus",
    NULL::"text" AS "establishmentMeans",
    NULL::"text" AS "recordNumber",
    NULL::"text" AS "populationTrend",
    NULL::"date" AS "eventDateStart",
    NULL::"date" AS "eventDateEnd",
    NULL::"uuid" AS "eventID",
    NULL::integer AS "samplingEffort",
    NULL::character varying(30) AS "sampleSizeUnit",
    NULL::numeric(10,1) AS "sampleSizeValue",
    NULL::character varying(100) AS "samplingProtocol",
    NULL::double precision AS "year",
    NULL::"text" AS "occurrenceID",
    NULL::double precision AS "decimalLatitude",
    NULL::double precision AS "decimalLongitude",
    NULL::character varying(50) AS "waterBody",
    NULL::"uuid" AS "locationID",
    NULL::integer AS "no_vatn_lnr",
    NULL::character varying(25) AS "municipality",
    NULL::character varying(25) AS "county",
    NULL::character(2) AS "countryCode",
    NULL::"text" AS "datasetID",
    NULL::"text" AS "datasetName",
    NULL::"text" AS "rightsHolder",
    NULL::"text" AS "accessRights",
    NULL::"text" AS "bibliographicCitation",
    NULL::"public"."geometry"(MultiPoint,25833) AS "geom",
    NULL::integer AS "waterBodyID";


--
-- Name: view_occurrence_pike; Type: VIEW; Schema: nofa; Owner: -
--

CREATE VIEW "nofa"."view_occurrence_pike" AS
 SELECT "row_number"() OVER () AS "GID",
    "l_taxon"."scientificName",
    "l_taxon"."vernacularName",
    "l_taxon"."vernacularName_NO",
    "sum"("occurrence"."individualCount") AS "individualCount",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."occurrenceStatus"), '|'::"text") AS "occurrenceStatus",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."establishmentMeans"), '|'::"text") AS "establishmentMeans",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."recordNumber"), '|'::"text") AS "recordNumber",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."populationTrend"), '|'::"text") AS "populationTrend",
    "array_to_string"("array_agg"(DISTINCT "event"."dateEnd"), '|'::"text") AS "eventDate",
    "string_agg"(("occurrence"."occurrenceID")::"text", ' | '::"text") AS "occurrenceID",
    "location"."decimalLatitude",
    "location"."decimalLongitude",
    "location"."waterBody",
    "location"."locationID",
    "location"."no_vatn_lnr",
    "location"."municipality",
    "location"."county",
    "array_to_string"("array_agg"(DISTINCT "event"."datasetID"), '|'::"text") AS "datasetID",
    "array_to_string"("array_agg"(DISTINCT "m_dataset"."datasetName"), '|'::"text") AS "datasetName",
    "array_to_string"("array_agg"(DISTINCT "m_dataset"."rightsHolder"), '|'::"text") AS "rightsHolder",
    "array_to_string"("array_agg"(DISTINCT "m_dataset"."accessRights"), '|'::"text") AS "accessRights",
    "array_to_string"("array_agg"(DISTINCT "m_dataset"."bibliographicCitation"), '|'::"text") AS "bibliographicCitation",
    "location"."geom",
    "location"."waterBodyID"
   FROM (((("nofa"."occurrence"
     JOIN "nofa"."event" ON (("occurrence"."eventID" = "event"."eventID")))
     JOIN "nofa"."location" ON (("event"."locationID" = "location"."locationID")))
     JOIN "nofa"."m_dataset" ON ((("event"."datasetID")::"text" = ("m_dataset"."datasetID")::"text")))
     JOIN "nofa"."l_taxon" ON (("occurrence"."taxonID" = "l_taxon"."taxonID")))
  WHERE (("l_taxon"."scientificName")::"text" = 'Esox lucius'::"text")
  GROUP BY "event"."locationID", "l_taxon"."scientificName", "l_taxon"."vernacularName", "l_taxon"."vernacularName_NO", "event"."datasetID", "location"."decimalLatitude", "location"."decimalLongitude", "location"."municipality", "location"."county", "location"."waterBody", "location"."locationID", "location"."no_vatn_lnr", "location"."geom", "location"."waterBodyID";


--
-- Name: view_occurrence_raw; Type: VIEW; Schema: nofa; Owner: -
--

CREATE VIEW "nofa"."view_occurrence_raw" AS
 SELECT "row_number"() OVER () AS "GID",
    "array_to_string"("array_agg"(DISTINCT "l_taxon"."scientificName"), '|'::"text") AS "scientificName",
    "array_to_string"("array_agg"(DISTINCT "l_taxon"."vernacularName"), '|'::"text") AS "vernacularName",
    "array_to_string"("array_agg"(DISTINCT "l_taxon"."vernacularName_NO"), '|'::"text") AS "vernacularName_NO",
    "occurrence"."individualCount",
    "occurrence"."occurrenceStatus",
    "occurrence"."establishmentMeans",
    "occurrence"."recordNumber",
    "occurrence"."populationTrend",
    "occurrence"."occurrenceRemarks",
    "occurrence"."establishmentRemarks",
    "array_to_string"("array_agg"(DISTINCT "event"."dateEnd"), '|'::"text") AS "dateEnd",
    "array_to_string"("array_agg"(DISTINCT "event"."dateStart"), '|'::"text") AS "dateStart",
    "occurrence"."occurrenceID",
    "array_to_string"("array_agg"(DISTINCT "location"."decimalLatitude"), '|'::"text") AS "decimalLatitude",
    "array_to_string"("array_agg"(DISTINCT "location"."decimalLongitude"), '|'::"text") AS "decimalLongitude",
    "array_to_string"("array_agg"(DISTINCT "location"."waterBody"), '|'::"text") AS "waterBody",
    "array_to_string"("array_agg"(DISTINCT "location"."locationID"), '|'::"text") AS "locationID",
    "array_to_string"("array_agg"(DISTINCT "location"."no_vatn_lnr"), '|'::"text") AS "no_vatn_lnr",
    "array_to_string"("array_agg"(DISTINCT "location"."municipality"), '|'::"text") AS "municipality",
    "array_to_string"("array_agg"(DISTINCT "location"."county"), '|'::"text") AS "county",
    "array_to_string"("array_agg"(DISTINCT "event"."datasetID"), '|'::"text") AS "datasetID",
    "array_to_string"("array_agg"(DISTINCT "m_dataset"."datasetName"), '|'::"text") AS "datasetName",
    "array_to_string"("array_agg"(DISTINCT "m_dataset"."rightsHolder"), '|'::"text") AS "rightsHolder",
    "array_to_string"("array_agg"(DISTINCT "m_dataset"."accessRights"), '|'::"text") AS "accessRights",
    "array_to_string"("array_agg"(DISTINCT "m_dataset"."bibliographicCitation"), '|'::"text") AS "bibliographicCitation",
    "location"."geom",
    "location"."waterBodyID"
   FROM (((("nofa"."occurrence"
     JOIN "nofa"."event" ON (("occurrence"."eventID" = "event"."eventID")))
     JOIN "nofa"."location" ON (("event"."locationID" = "location"."locationID")))
     JOIN "nofa"."m_dataset" ON ((("event"."datasetID")::"text" = ("m_dataset"."datasetID")::"text")))
     JOIN "nofa"."l_taxon" ON (("occurrence"."taxonID" = "l_taxon"."taxonID")))
  GROUP BY "occurrence"."individualCount", "occurrence"."occurrenceStatus", "occurrence"."establishmentMeans", "occurrence"."recordNumber", "occurrence"."populationTrend", "occurrence"."occurrenceID", "location"."geom", "occurrence"."occurrenceRemarks", "occurrence"."establishmentRemarks", "location"."waterBodyID";


--
-- Name: view_occurrence_roach; Type: VIEW; Schema: nofa; Owner: -
--

CREATE VIEW "nofa"."view_occurrence_roach" AS
 SELECT "row_number"() OVER () AS "GID",
    "l_taxon"."scientificName",
    "l_taxon"."vernacularName",
    "l_taxon"."vernacularName_NO",
    "sum"("occurrence"."individualCount") AS "individualCount",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."occurrenceStatus"), '|'::"text") AS "occurrenceStatus",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."establishmentMeans"), '|'::"text") AS "establishmentMeans",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."recordNumber"), '|'::"text") AS "recordNumber",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."populationTrend"), '|'::"text") AS "populationTrend",
    "array_to_string"("array_agg"(DISTINCT "event"."dateEnd"), '|'::"text") AS "eventDate",
    "string_agg"(("occurrence"."occurrenceID")::"text", ' | '::"text") AS "occurrenceID",
    "location"."decimalLatitude",
    "location"."decimalLongitude",
    "location"."waterBody",
    "location"."locationID",
    "location"."no_vatn_lnr",
    "location"."municipality",
    "location"."county",
    "array_to_string"("array_agg"(DISTINCT "event"."datasetID"), '|'::"text") AS "datasetID",
    "array_to_string"("array_agg"(DISTINCT "m_dataset"."datasetName"), '|'::"text") AS "datasetName",
    "array_to_string"("array_agg"(DISTINCT "m_dataset"."rightsHolder"), '|'::"text") AS "rightsHolder",
    "array_to_string"("array_agg"(DISTINCT "m_dataset"."accessRights"), '|'::"text") AS "accessRights",
    "array_to_string"("array_agg"(DISTINCT "m_dataset"."bibliographicCitation"), '|'::"text") AS "bibliographicCitation",
    "location"."geom",
    "location"."waterBodyID"
   FROM (((("nofa"."occurrence"
     JOIN "nofa"."event" ON (("occurrence"."eventID" = "event"."eventID")))
     JOIN "nofa"."location" ON (("event"."locationID" = "location"."locationID")))
     JOIN "nofa"."m_dataset" ON ((("event"."datasetID")::"text" = ("m_dataset"."datasetID")::"text")))
     JOIN "nofa"."l_taxon" ON (("occurrence"."taxonID" = "l_taxon"."taxonID")))
  WHERE (("l_taxon"."scientificName")::"text" = 'Rutilus rutilus'::"text")
  GROUP BY "event"."locationID", "l_taxon"."scientificName", "l_taxon"."vernacularName", "l_taxon"."vernacularName_NO", "event"."datasetID", "location"."decimalLatitude", "location"."decimalLongitude", "location"."municipality", "location"."county", "location"."waterBody", "location"."locationID", "location"."no_vatn_lnr", "location"."geom", "location"."waterBodyID";


--
-- Name: view_occurrences_agr_by_location; Type: VIEW; Schema: nofa; Owner: -
--

CREATE VIEW "nofa"."view_occurrences_agr_by_location" AS
 SELECT "row_number"() OVER () AS "GID",
    "l_taxon"."scientificName",
    "l_taxon"."vernacularName",
    "l_taxon"."vernacularName_NO",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."occurrenceStatus"), '|'::"text") AS "occurrenceStatus",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."establishmentMeans"), '|'::"text") AS "establishmentMeans",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."recordNumber"), '|'::"text") AS "recordNumber",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."populationTrend"), '|'::"text") AS "populationTrend",
    "array_to_string"("array_agg"(DISTINCT "event"."dateEnd"), '|'::"text") AS "dateEnd",
    "array_to_string"("array_agg"(DISTINCT "event"."dateStart"), '|'::"text") AS "dateStart",
    "min"("event"."dateStart") AS "firstDate",
    "max"("event"."dateEnd") AS "lastDate",
    "date_part"('year'::"text", "max"("event"."dateEnd")) AS "lastYear",
    "date_part"('year'::"text", "min"("event"."dateStart")) AS "firstYear",
    "array_to_string"("array_agg"(DISTINCT "event"."eventID"), '|'::"text") AS "eventID",
    "array_to_string"("array_agg"(DISTINCT "date_part"('year'::"text", "event"."dateEnd")), '|'::"text") AS "years",
    "array_to_string"("array_agg"(DISTINCT "event"."eventDateQualifier"), '|'::"text") AS "eventDateQualifier",
    "array_to_string"("array_agg"(DISTINCT "event"."recordedBy"), '|'::"text") AS "recordedBy",
    "string_agg"(("occurrence"."occurrenceID")::"text", ' | '::"text") AS "occurrenceID",
    "location"."decimalLatitude",
    "location"."decimalLongitude",
    "location"."waterBody",
    "location"."locationID",
    "location"."no_vatn_lnr",
    "location"."municipality",
    "location"."county",
    "location"."countryCode",
    "array_to_string"("array_agg"(DISTINCT "event"."datasetID"), '|'::"text") AS "datasetID",
    "array_to_string"("array_agg"(DISTINCT "m_dataset"."datasetName"), '|'::"text") AS "datasetName",
    "array_to_string"("array_agg"(DISTINCT "m_dataset"."rightsHolder"), '|'::"text") AS "rightsHolder",
    "array_to_string"("array_agg"(DISTINCT "m_dataset"."accessRights"), '|'::"text") AS "accessRights",
    "array_to_string"("array_agg"(DISTINCT "m_dataset"."bibliographicCitation"), '|'::"text") AS "bibliographicCitation",
    "location"."geom",
    "location"."waterBodyID"
   FROM (((("nofa"."occurrence"
     JOIN "nofa"."event" ON (("occurrence"."eventID" = "event"."eventID")))
     JOIN "nofa"."location" ON (("event"."locationID" = "location"."locationID")))
     JOIN "nofa"."m_dataset" ON ((("event"."datasetID")::"text" = ("m_dataset"."datasetID")::"text")))
     JOIN "nofa"."l_taxon" ON (("occurrence"."taxonID" = "l_taxon"."taxonID")))
  GROUP BY "l_taxon"."scientificName", "l_taxon"."vernacularName", "l_taxon"."vernacularName_NO", "location"."decimalLatitude", "location"."decimalLongitude", "location"."municipality", "location"."county", "location"."countryCode", "location"."waterBody", "location"."locationID", "location"."no_vatn_lnr", "location"."geom", "location"."waterBodyID";


--
-- Name: dataset_log; Type: TABLE; Schema: plugin; Owner: -
--

CREATE TABLE "plugin"."dataset_log" (
    "dataset_id" character varying(255) NOT NULL,
    "test" boolean DEFAULT false,
    "username" "text" NOT NULL,
    "insert_timestamp" timestamp without time zone DEFAULT "now"(),
    "update_timestamp" timestamp without time zone DEFAULT "now"()
);


--
-- Name: TABLE "dataset_log"; Type: COMMENT; Schema: plugin; Owner: -
--

COMMENT ON TABLE "plugin"."dataset_log" IS 'Log table storing info about all new dataset metadata inserted by NOFAInsert plugin.';


--
-- Name: event_log; Type: TABLE; Schema: plugin; Owner: -
--

CREATE TABLE "plugin"."event_log" (
    "event_id" "uuid" NOT NULL,
    "dataset_id" "text" NOT NULL,
    "project_id" "text" NOT NULL,
    "reference_id" integer,
    "location_id" "text" NOT NULL,
    "test" boolean DEFAULT false,
    "username" "text" NOT NULL,
    "insert_timestamp" timestamp without time zone DEFAULT "now"(),
    "update_timestamp" timestamp without time zone DEFAULT "now"()
);


--
-- Name: TABLE "event_log"; Type: COMMENT; Schema: plugin; Owner: -
--

COMMENT ON TABLE "plugin"."event_log" IS 'Log table storing info about all new events inserted by NOFAInsert plugin.';


--
-- Name: location_log; Type: TABLE; Schema: plugin; Owner: -
--

CREATE TABLE "plugin"."location_log" (
    "location_id" "uuid" NOT NULL,
    "test" boolean DEFAULT false,
    "username" "text" NOT NULL,
    "location_name" "text",
    "insert_timestamp" timestamp without time zone DEFAULT "now"(),
    "update_timestamp" timestamp without time zone DEFAULT "now"()
);


--
-- Name: TABLE "location_log"; Type: COMMENT; Schema: plugin; Owner: -
--

COMMENT ON TABLE "plugin"."location_log" IS 'Log table storing info about all new locations inserted by NOFAInsert plugin.';


--
-- Name: occurrence_log; Type: TABLE; Schema: plugin; Owner: -
--

CREATE TABLE "plugin"."occurrence_log" (
    "occurrence_log_id" integer NOT NULL,
    "occurrence_id" "text" NOT NULL,
    "event_id" "text" NOT NULL,
    "dataset_id" "text" NOT NULL,
    "project_id" "text" NOT NULL,
    "reference_id" integer,
    "location_id" "text" NOT NULL,
    "test" boolean DEFAULT false,
    "username" "text" NOT NULL,
    "insert_timestamp" timestamp without time zone DEFAULT "now"(),
    "update_timestamp" timestamp without time zone DEFAULT "now"()
);


--
-- Name: TABLE "occurrence_log"; Type: COMMENT; Schema: plugin; Owner: -
--

COMMENT ON TABLE "plugin"."occurrence_log" IS 'Log table storing info about all new occurrences inserted by NOFAInsert plugin.';


--
-- Name: occurrence_log_occurrence_log_id_seq; Type: SEQUENCE; Schema: plugin; Owner: -
--

CREATE SEQUENCE "plugin"."occurrence_log_occurrence_log_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: occurrence_log_occurrence_log_id_seq; Type: SEQUENCE OWNED BY; Schema: plugin; Owner: -
--

ALTER SEQUENCE "plugin"."occurrence_log_occurrence_log_id_seq" OWNED BY "plugin"."occurrence_log"."occurrence_log_id";


--
-- Name: project_log; Type: TABLE; Schema: plugin; Owner: -
--

CREATE TABLE "plugin"."project_log" (
    "project_id" integer NOT NULL,
    "test" boolean DEFAULT false,
    "username" "text" NOT NULL,
    "insert_timestamp" timestamp without time zone DEFAULT "now"(),
    "update_timestamp" timestamp without time zone DEFAULT "now"()
);


--
-- Name: TABLE "project_log"; Type: COMMENT; Schema: plugin; Owner: -
--

COMMENT ON TABLE "plugin"."project_log" IS 'Log table storing info about all new project metadata inserted by NOFAInsert plugin.';


--
-- Name: reference_log; Type: TABLE; Schema: plugin; Owner: -
--

CREATE TABLE "plugin"."reference_log" (
    "reference_id" integer NOT NULL,
    "test" boolean DEFAULT false,
    "username" "text" NOT NULL,
    "insert_timestamp" timestamp without time zone DEFAULT "now"(),
    "update_timestamp" timestamp without time zone DEFAULT "now"()
);


--
-- Name: TABLE "reference_log"; Type: COMMENT; Schema: plugin; Owner: -
--

COMMENT ON TABLE "plugin"."reference_log" IS 'Log table storing info about all new reference metadata inserted by NOFAInsert plugin.';


--
-- Name: l_populationTrend populationTrendID; Type: DEFAULT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."l_populationTrend" ALTER COLUMN "populationTrendID" SET DEFAULT "nextval"('"nofa"."l_populationTrend_populationTrendID_seq"'::"regclass");


--
-- Name: l_taxon_historicDistribution localityID; Type: DEFAULT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."l_taxon_historicDistribution" ALTER COLUMN "localityID" SET DEFAULT "nextval"('"nofa"."l_taxon_historicDistribution_localityID_seq"'::"regclass");


--
-- Name: lake id; Type: DEFAULT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."lake" ALTER COLUMN "id" SET DEFAULT "nextval"('"nofa"."location_id_seq"'::"regclass");


--
-- Name: lake isBorder; Type: DEFAULT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."lake" ALTER COLUMN "isBorder" SET DEFAULT false;


--
-- Name: m_project projectID; Type: DEFAULT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."m_project" ALTER COLUMN "projectID" SET DEFAULT "nextval"('"nofa"."a_prosjekt_prosjekt_id_seq"'::"regclass");


--
-- Name: m_reference referenceID; Type: DEFAULT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."m_reference" ALTER COLUMN "referenceID" SET DEFAULT "nextval"('"nofa"."a_referanse_referanse_id_seq"'::"regclass");


--
-- Name: otherwaterbody id; Type: DEFAULT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."otherwaterbody" ALTER COLUMN "id" SET DEFAULT "nextval"('"nofa"."location_id_seq"'::"regclass");


--
-- Name: otherwaterbody isBorder; Type: DEFAULT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."otherwaterbody" ALTER COLUMN "isBorder" SET DEFAULT false;


--
-- Name: stream id; Type: DEFAULT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."stream" ALTER COLUMN "id" SET DEFAULT "nextval"('"nofa"."location_id_seq"'::"regclass");


--
-- Name: stream isBorder; Type: DEFAULT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."stream" ALTER COLUMN "isBorder" SET DEFAULT false;


--
-- Name: waterBody id; Type: DEFAULT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."waterBody" ALTER COLUMN "id" SET DEFAULT "nextval"('"nofa"."location_id_seq"'::"regclass");


--
-- Name: occurrence_log occurrence_log_id; Type: DEFAULT; Schema: plugin; Owner: -
--

ALTER TABLE ONLY "plugin"."occurrence_log" ALTER COLUMN "occurrence_log_id" SET DEFAULT "nextval"('"plugin"."occurrence_log_occurrence_log_id_seq"'::"regclass");


--
-- Name: event event_eventID_uniq; Type: CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."event"
    ADD CONSTRAINT "event_eventID_uniq" UNIQUE ("eventID");


--
-- Name: event event_pkey; Type: CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."event"
    ADD CONSTRAINT "event_pkey" PRIMARY KEY ("eventID");


--
-- Name: l_basisOfRecord l_basisofrecord_pkey; Type: CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."l_basisOfRecord"
    ADD CONSTRAINT "l_basisofrecord_pkey" PRIMARY KEY ("basisOfRecord");


--
-- Name: l_ecotype l_ecotype_pkey; Type: CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."l_ecotype"
    ADD CONSTRAINT "l_ecotype_pkey" PRIMARY KEY ("ecotypeID");


--
-- Name: l_establishmentMeans l_establishmentMeans_pkey; Type: CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."l_establishmentMeans"
    ADD CONSTRAINT "l_establishmentMeans_pkey" PRIMARY KEY ("establishmentMeans");


--
-- Name: l_eventDateQualifier l_eventdatequalifier_pk; Type: CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."l_eventDateQualifier"
    ADD CONSTRAINT "l_eventdatequalifier_pk" PRIMARY KEY ("eventDateQualifier");


--
-- Name: l_lifeStage l_lifeStage_pkey; Type: CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."l_lifeStage"
    ADD CONSTRAINT "l_lifeStage_pkey" PRIMARY KEY ("lifeStage");


--
-- Name: l_locationType l_locationtype_pkey; Type: CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."l_locationType"
    ADD CONSTRAINT "l_locationtype_pkey" PRIMARY KEY ("locationType");


--
-- Name: l_mof_occurrence l_mof_occurrence_pkey; Type: CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."l_mof_occurrence"
    ADD CONSTRAINT "l_mof_occurrence_pkey" PRIMARY KEY ("measurementType");


--
-- Name: l_occurrenceStatus l_occurrencestatus_pk; Type: CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."l_occurrenceStatus"
    ADD CONSTRAINT "l_occurrencestatus_pk" PRIMARY KEY ("occurrenceStatus");


--
-- Name: l_organismQuantityType l_organismQuantityType_pkey; Type: CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."l_organismQuantityType"
    ADD CONSTRAINT "l_organismQuantityType_pkey" PRIMARY KEY ("organismQuantityType");


--
-- Name: l_populationTrend l_populationtrend_pk; Type: CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."l_populationTrend"
    ADD CONSTRAINT "l_populationtrend_pk" PRIMARY KEY ("populationTrendID");


--
-- Name: l_populationTrend l_populationtrend_unique_populationtrend; Type: CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."l_populationTrend"
    ADD CONSTRAINT "l_populationtrend_unique_populationtrend" UNIQUE ("populationTrend");


--
-- Name: m_project l_projectID_uniq; Type: CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."m_project"
    ADD CONSTRAINT "l_projectID_uniq" UNIQUE ("projectID");


--
-- Name: l_referenceType l_referanseTypeID_uniq; Type: CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."l_referenceType"
    ADD CONSTRAINT "l_referanseTypeID_uniq" UNIQUE ("referanseTypeID");


--
-- Name: m_reference l_referenceID_uniq; Type: CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."m_reference"
    ADD CONSTRAINT "l_referenceID_uniq" UNIQUE ("referenceID");


--
-- Name: l_referenceType l_referenceType_pkey; Type: CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."l_referenceType"
    ADD CONSTRAINT "l_referenceType_pkey" PRIMARY KEY ("referanseTypeID");


--
-- Name: l_referenceType l_referencetype_unique_referencetype; Type: CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."l_referenceType"
    ADD CONSTRAINT "l_referencetype_unique_referencetype" UNIQUE ("referenceType");


--
-- Name: l_reliability l_reliability_pk; Type: CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."l_reliability"
    ADD CONSTRAINT "l_reliability_pk" PRIMARY KEY ("reliability");


--
-- Name: l_reproductiveCondition l_reproductiveCondition_pkey; Type: CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."l_reproductiveCondition"
    ADD CONSTRAINT "l_reproductiveCondition_pkey" PRIMARY KEY ("reproductiveCondition");


--
-- Name: l_rettighetshaver l_rettighetshaver_pkey; Type: CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."l_rettighetshaver"
    ADD CONSTRAINT "l_rettighetshaver_pkey" PRIMARY KEY ("rettighetshaver_id");


--
-- Name: l_rettighetshaver l_rettighetshaver_rettighetshaver_id_uniq; Type: CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."l_rettighetshaver"
    ADD CONSTRAINT "l_rettighetshaver_rettighetshaver_id_uniq" UNIQUE ("rettighetshaver_id");


--
-- Name: l_sampleSizeUnit l_sampleSizeUnit_pkey; Type: CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."l_sampleSizeUnit"
    ADD CONSTRAINT "l_sampleSizeUnit_pkey" PRIMARY KEY ("sampleSizeUnit");


--
-- Name: l_samplingProtocol l_samplingProtocol_pkey; Type: CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."l_samplingProtocol"
    ADD CONSTRAINT "l_samplingProtocol_pkey" PRIMARY KEY ("samplingProtocol");


--
-- Name: l_sex l_sex_pkey; Type: CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."l_sex"
    ADD CONSTRAINT "l_sex_pkey" PRIMARY KEY ("sex");


--
-- Name: l_spawningCondition l_spawningCondition_pkey; Type: CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."l_spawningCondition"
    ADD CONSTRAINT "l_spawningCondition_pkey" PRIMARY KEY ("spawningCondition");


--
-- Name: l_spawningLocation l_spawningLocation_pkey; Type: CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."l_spawningLocation"
    ADD CONSTRAINT "l_spawningLocation_pkey" PRIMARY KEY ("spawningLocation");


--
-- Name: l_taxonSlopeEffect l_taxonSlopeEffect_pkey; Type: CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."l_taxonSlopeEffect"
    ADD CONSTRAINT "l_taxonSlopeEffect_pkey" PRIMARY KEY ("taxonID");


--
-- Name: l_taxon_historicDistribution l_taxon_historicDistribution_pkey; Type: CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."l_taxon_historicDistribution"
    ADD CONSTRAINT "l_taxon_historicDistribution_pkey" PRIMARY KEY ("localityID");


--
-- Name: l_taxon l_taxon_pkey; Type: CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."l_taxon"
    ADD CONSTRAINT "l_taxon_pkey" PRIMARY KEY ("taxonID");


--
-- Name: l_taxon l_taxon_scientificname_unique; Type: CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."l_taxon"
    ADD CONSTRAINT "l_taxon_scientificname_unique" UNIQUE ("scientificName");


--
-- Name: location location_pkey; Type: CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."location"
    ADD CONSTRAINT "location_pkey" PRIMARY KEY ("locationID");


--
-- Name: m_dataset m_dataset_pkey; Type: CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."m_dataset"
    ADD CONSTRAINT "m_dataset_pkey" PRIMARY KEY ("datasetID");


--
-- Name: m_project m_project_pkey; Type: CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."m_project"
    ADD CONSTRAINT "m_project_pkey" PRIMARY KEY ("projectID");


--
-- Name: m_reference m_reference_pkey; Type: CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."m_reference"
    ADD CONSTRAINT "m_reference_pkey" PRIMARY KEY ("referenceID");


--
-- Name: mapping_info mapping_info_pkey; Type: CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."mapping_info"
    ADD CONSTRAINT "mapping_info_pkey" PRIMARY KEY ("mapping_infoID");


--
-- Name: mof_occurrence mof_occurrence_pkey; Type: CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."mof_occurrence"
    ADD CONSTRAINT "mof_occurrence_pkey" PRIMARY KEY ("measurementID");


--
-- Name: lake nofa_waterBody_lake_pk; Type: CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."lake"
    ADD CONSTRAINT "nofa_waterBody_lake_pk" PRIMARY KEY ("id");


--
-- Name: otherwaterbody nofa_waterBody_otherWaterbody_pk; Type: CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."otherwaterbody"
    ADD CONSTRAINT "nofa_waterBody_otherWaterbody_pk" PRIMARY KEY ("id");


--
-- Name: stream nofa_waterBody_stream_pk; Type: CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."stream"
    ADD CONSTRAINT "nofa_waterBody_stream_pk" PRIMARY KEY ("id");


--
-- Name: occurrence occurrenceID_pkey; Type: CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."occurrence"
    ADD CONSTRAINT "occurrenceID_pkey" PRIMARY KEY ("occurrenceID");


--
-- Name: occurrence occurrenceID_uniq; Type: CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."occurrence"
    ADD CONSTRAINT "occurrenceID_uniq" UNIQUE ("occurrenceID");


--
-- Name: samplingTaxaRange samplingTaxaRange_pkey; Type: CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."samplingTaxaRange"
    ADD CONSTRAINT "samplingTaxaRange_pkey" PRIMARY KEY ("id");


--
-- Name: waterBody waterbody_pkey; Type: CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."waterBody"
    ADD CONSTRAINT "waterbody_pkey" PRIMARY KEY ("id");


--
-- Name: l_taxon_historicdistribution_spidx; Type: INDEX; Schema: nofa; Owner: -
--

CREATE INDEX "l_taxon_historicdistribution_spidx" ON "nofa"."l_taxon_historicDistribution" USING "gist" ("geom");


--
-- Name: l_taxon_historicdistribution_taxidx; Type: INDEX; Schema: nofa; Owner: -
--

CREATE INDEX "l_taxon_historicdistribution_taxidx" ON "nofa"."l_taxon_historicDistribution" USING "gist" ("geom");


--
-- Name: nofa_event_idx_locationid; Type: INDEX; Schema: nofa; Owner: -
--

CREATE INDEX "nofa_event_idx_locationid" ON "nofa"."event" USING "btree" ("locationID");


--
-- Name: nofa_event_idx_parenteventid; Type: INDEX; Schema: nofa; Owner: -
--

CREATE INDEX "nofa_event_idx_parenteventid" ON "nofa"."event" USING "btree" ("parentEventID");


--
-- Name: nofa_event_idx_projectid; Type: INDEX; Schema: nofa; Owner: -
--

CREATE INDEX "nofa_event_idx_projectid" ON "nofa"."event" USING "btree" ("projectID");


--
-- Name: nofa_event_idx_referenceid; Type: INDEX; Schema: nofa; Owner: -
--

CREATE INDEX "nofa_event_idx_referenceid" ON "nofa"."event" USING "btree" ("referenceID");


--
-- Name: nofa_event_idx_reliability; Type: INDEX; Schema: nofa; Owner: -
--

CREATE INDEX "nofa_event_idx_reliability" ON "nofa"."event" USING "btree" ("reliability");


--
-- Name: nofa_event_idx_samplingprotocol; Type: INDEX; Schema: nofa; Owner: -
--

CREATE INDEX "nofa_event_idx_samplingprotocol" ON "nofa"."event" USING "btree" ("samplingProtocol");


--
-- Name: nofa_lakes_sidx_geom; Type: INDEX; Schema: nofa; Owner: -
--

CREATE INDEX "nofa_lakes_sidx_geom" ON "nofa"."lake" USING "gist" ("geom");


--
-- Name: nofa_location_idx_country; Type: INDEX; Schema: nofa; Owner: -
--

CREATE INDEX "nofa_location_idx_country" ON "nofa"."location" USING "btree" ("countryCode");


--
-- Name: nofa_location_idx_county; Type: INDEX; Schema: nofa; Owner: -
--

CREATE INDEX "nofa_location_idx_county" ON "nofa"."location" USING "btree" ("county");


--
-- Name: nofa_location_idx_ebint; Type: INDEX; Schema: nofa; Owner: -
--

CREATE INDEX "nofa_location_idx_ebint" ON "nofa"."location" USING "btree" ("ebint");


--
-- Name: nofa_location_idx_fi_nro; Type: INDEX; Schema: nofa; Owner: -
--

CREATE INDEX "nofa_location_idx_fi_nro" ON "nofa"."location" USING "btree" ("fi_nro");


--
-- Name: nofa_location_idx_municipality; Type: INDEX; Schema: nofa; Owner: -
--

CREATE INDEX "nofa_location_idx_municipality" ON "nofa"."location" USING "btree" ("municipality");


--
-- Name: nofa_location_idx_no_vatn_lnr; Type: INDEX; Schema: nofa; Owner: -
--

CREATE INDEX "nofa_location_idx_no_vatn_lnr" ON "nofa"."location" USING "btree" ("no_vatn_lnr");


--
-- Name: nofa_location_idx_se_lake_id; Type: INDEX; Schema: nofa; Owner: -
--

CREATE INDEX "nofa_location_idx_se_lake_id" ON "nofa"."location" USING "btree" ("se_lake_id");


--
-- Name: nofa_location_idx_se_sjoid; Type: INDEX; Schema: nofa; Owner: -
--

CREATE INDEX "nofa_location_idx_se_sjoid" ON "nofa"."location" USING "btree" ("se_sjoid");


--
-- Name: nofa_location_idx_waterbodyid; Type: INDEX; Schema: nofa; Owner: -
--

CREATE INDEX "nofa_location_idx_waterbodyid" ON "nofa"."location" USING "btree" ("waterBodyID");


--
-- Name: nofa_location_sidx_geom; Type: INDEX; Schema: nofa; Owner: -
--

CREATE INDEX "nofa_location_sidx_geom" ON "nofa"."location" USING "gist" ("geom");


--
-- Name: nofa_occurrence_idx_establishmentmeans; Type: INDEX; Schema: nofa; Owner: -
--

CREATE INDEX "nofa_occurrence_idx_establishmentmeans" ON "nofa"."occurrence" USING "btree" ("establishmentMeans");


--
-- Name: nofa_occurrence_idx_eventid; Type: INDEX; Schema: nofa; Owner: -
--

CREATE INDEX "nofa_occurrence_idx_eventid" ON "nofa"."occurrence" USING "btree" ("eventID");


--
-- Name: nofa_occurrence_idx_occurrencestatus; Type: INDEX; Schema: nofa; Owner: -
--

CREATE INDEX "nofa_occurrence_idx_occurrencestatus" ON "nofa"."occurrence" USING "btree" ("occurrenceStatus");


--
-- Name: nofa_occurrence_idx_populationtrend; Type: INDEX; Schema: nofa; Owner: -
--

CREATE INDEX "nofa_occurrence_idx_populationtrend" ON "nofa"."occurrence" USING "btree" ("populationTrend");


--
-- Name: nofa_occurrence_idx_taxonid; Type: INDEX; Schema: nofa; Owner: -
--

CREATE INDEX "nofa_occurrence_idx_taxonid" ON "nofa"."occurrence" USING "btree" ("taxonID");


--
-- Name: nofa_streams_sidx_geom; Type: INDEX; Schema: nofa; Owner: -
--

CREATE INDEX "nofa_streams_sidx_geom" ON "nofa"."stream" USING "btree" ("geom");


--
-- Name: nofa_waterbody_pkey; Type: INDEX; Schema: nofa; Owner: -
--

CREATE UNIQUE INDEX "nofa_waterbody_pkey" ON "nofa"."waterBody" USING "btree" ("id");


--
-- Name: nofa_waterbodys_sidx_geom_centroid; Type: INDEX; Schema: nofa; Owner: -
--

CREATE INDEX "nofa_waterbodys_sidx_geom_centroid" ON "nofa"."waterBody" USING "gist" ("centroid");


--
-- Name: simulation_out_test_waterBodyID; Type: INDEX; Schema: nofa; Owner: -
--

CREATE INDEX "simulation_out_test_waterBodyID" ON "nofa"."simulation_out_test" USING "btree" ("waterBodyID");


--
-- Name: view_occurrence_by_event _RETURN; Type: RULE; Schema: nofa; Owner: -
--

CREATE OR REPLACE VIEW "nofa"."view_occurrence_by_event" AS
 SELECT "row_number"() OVER () AS "OID",
    "l_taxon"."scientificName",
    "l_taxon"."vernacularName",
    "l_taxon"."vernacularName_NO",
    "sum"("occurrence"."individualCount") AS "individualCount",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."occurrenceStatus"), '|'::"text") AS "occurrenceStatus",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."establishmentMeans"), '|'::"text") AS "establishmentMeans",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."recordNumber"), '|'::"text") AS "recordNumber",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."populationTrend"), '|'::"text") AS "populationTrend",
    "string_agg"(("occurrence"."occurrenceID")::"text", ' | '::"text") AS "occurrenceID",
    "event"."dateEnd",
    "date_part"('year'::"text", "event"."dateEnd") AS "year",
    "event"."eventID",
    "event"."samplingTaxaRange",
    "event"."samplingProtocol",
    "event"."recordedBy",
    "event"."reliability",
    "location"."locationID",
    "location"."decimalLatitude",
    "location"."decimalLongitude",
    "location"."waterBody",
    "location"."no_vatn_lnr" AS "vatn_lnr",
    "location"."municipality",
    "location"."county",
    "location"."countryCode",
    "public"."st_x"("public"."st_centroid"("location"."geom")) AS "utm_x",
    "public"."st_y"("public"."st_centroid"("location"."geom")) AS "utm_y",
    "location"."waterBodyID",
    "location"."minimumElevationInMeters",
    "location"."maximumElevationInMeters",
    "m_dataset"."datasetName",
    "m_dataset"."rightsHolder",
    "m_dataset"."datasetID",
    "m_dataset"."bibliographicCitation",
    "lake"."area_km2",
    "lake"."distance_to_road",
    "lake"."perimeter_m",
    "lake"."elevation",
    "lb"."buffer_5000m_population_2006",
    "lb"."buffer_5000m_population_2011",
    "location_EuroLST_BioClim"."eurolst_bio01",
    "location_EuroLST_BioClim"."eurolst_bio02",
    "location_EuroLST_BioClim"."eurolst_bio03",
    "location_EuroLST_BioClim"."eurolst_bio05",
    "location_EuroLST_BioClim"."eurolst_bio06",
    "location_EuroLST_BioClim"."eurolst_bio07",
    "location_EuroLST_BioClim"."eurolst_bio10",
    "location_EuroLST_BioClim"."eurolst_bio11",
    "location"."geom"
   FROM ((((((("nofa"."occurrence"
     JOIN "nofa"."event" ON (("occurrence"."eventID" = "event"."eventID")))
     JOIN "nofa"."location" ON (("event"."locationID" = "location"."locationID")))
     JOIN "nofa"."m_dataset" ON ((("event"."datasetID")::"text" = ("m_dataset"."datasetID")::"text")))
     JOIN "nofa"."lake" ON (("location"."waterBodyID" = "lake"."id")))
     JOIN "environmental"."location_EuroLST_BioClim" ON (("location"."locationID" = "location_EuroLST_BioClim"."locationID")))
     JOIN "nofa"."l_taxon" ON (("occurrence"."taxonID" = "l_taxon"."taxonID")))
     JOIN ( SELECT "lake_buffer"."id",
            "lake_buffer"."buffer_5000m_population_2006",
            "lake_buffer"."buffer_5000m_population_2011"
           FROM "environmental"."lake_buffer") "lb" ON (("location"."waterBodyID" = "lb"."id")))
  GROUP BY "event"."dateEnd", "event"."locationID", "l_taxon"."scientificName", "l_taxon"."vernacularName", "l_taxon"."vernacularName_NO", "event"."datasetID", "location"."decimalLatitude", "location"."decimalLongitude", "location"."municipality", "event"."fieldNumber", "location"."locationID", "location"."county", "location"."geom", "location"."waterBodyID", "lake"."area_km2", "lake"."distance_to_road", "lake"."perimeter_m", "lake"."elevation", "location_EuroLST_BioClim"."eurolst_bio01", "location_EuroLST_BioClim"."eurolst_bio02", "location_EuroLST_BioClim"."eurolst_bio03", "location_EuroLST_BioClim"."eurolst_bio05", "location_EuroLST_BioClim"."eurolst_bio06", "location_EuroLST_BioClim"."eurolst_bio07", "location_EuroLST_BioClim"."eurolst_bio10", "location_EuroLST_BioClim"."eurolst_bio11", "event"."eventID", "event"."samplingTaxaRange", "event"."samplingProtocol", "event"."recordedBy", "event"."reliability", "location"."countryCode", "m_dataset"."datasetName", "m_dataset"."rightsHolder", "m_dataset"."datasetID", "m_dataset"."bibliographicCitation", "location"."waterBody", "location"."no_vatn_lnr", "lb"."buffer_5000m_population_2006", "lb"."buffer_5000m_population_2011";


--
-- Name: view_occurrence_by_event_all_samplingprotocol _RETURN; Type: RULE; Schema: nofa; Owner: -
--

CREATE OR REPLACE VIEW "nofa"."view_occurrence_by_event_all_samplingprotocol" AS
 SELECT "row_number"() OVER () AS "GID",
    "l_taxon"."scientificName",
    "l_taxon"."vernacularName",
    "l_taxon"."vernacularName_NO",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."occurrenceStatus"), '|'::"text") AS "occurrenceStatus",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."establishmentMeans"), '|'::"text") AS "establishmentMeans",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."recordNumber"), '|'::"text") AS "recordNumber",
    "array_to_string"("array_agg"(DISTINCT "occurrence"."populationTrend"), '|'::"text") AS "populationTrend",
    "event"."dateStart" AS "eventDateStart",
    "event"."dateEnd" AS "eventDateEnd",
    "event"."eventID",
    "event"."samplingEffort",
    "event"."sampleSizeUnit",
    "event"."sampleSizeValue",
    "event"."samplingProtocol",
    "date_part"('year'::"text", "event"."dateEnd") AS "year",
    "string_agg"(("occurrence"."occurrenceID")::"text", ' | '::"text") AS "occurrenceID",
    "location"."decimalLatitude",
    "location"."decimalLongitude",
    "location"."waterBody",
    "location"."locationID",
    "location"."no_vatn_lnr",
    "location"."municipality",
    "location"."county",
    "location"."countryCode",
    "array_to_string"("array_agg"(DISTINCT "event"."datasetID"), '|'::"text") AS "datasetID",
    "array_to_string"("array_agg"(DISTINCT "m_dataset"."datasetName"), '|'::"text") AS "datasetName",
    "array_to_string"("array_agg"(DISTINCT "m_dataset"."rightsHolder"), '|'::"text") AS "rightsHolder",
    "array_to_string"("array_agg"(DISTINCT "m_dataset"."accessRights"), '|'::"text") AS "accessRights",
    "array_to_string"("array_agg"(DISTINCT "m_dataset"."bibliographicCitation"), '|'::"text") AS "bibliographicCitation",
    "location"."geom",
    "location"."waterBodyID"
   FROM (((("nofa"."occurrence"
     JOIN "nofa"."event" ON (("occurrence"."eventID" = "event"."eventID")))
     JOIN "nofa"."location" ON (("event"."locationID" = "location"."locationID")))
     JOIN "nofa"."m_dataset" ON ((("event"."datasetID")::"text" = ("m_dataset"."datasetID")::"text")))
     JOIN "nofa"."l_taxon" ON (("occurrence"."taxonID" = "l_taxon"."taxonID")))
  GROUP BY "event"."eventID", "event"."dateEnd", "event"."locationID", "l_taxon"."scientificName", "l_taxon"."vernacularName", "l_taxon"."vernacularName_NO", "event"."datasetID", "location"."decimalLatitude", "location"."decimalLongitude", "location"."municipality", "location"."county", "location"."countryCode", "location"."waterBody", "location"."locationID", "location"."no_vatn_lnr", "location"."geom", "location"."waterBodyID";


--
-- Name: view_locations_raw view_locations_raw_update; Type: RULE; Schema: nofa; Owner: -
--

CREATE RULE "view_locations_raw_update" AS
    ON UPDATE TO "nofa"."view_locations_raw" DO INSTEAD ( UPDATE "nofa"."location" SET "locationType" = "new"."locationType"
  WHERE ("location"."locationID" = "old"."locationID");
 UPDATE "nofa"."location" SET "georeferenceRemarks" = "new"."georeferenceRemarks"
  WHERE ("location"."locationID" = "old"."locationID");
 UPDATE "nofa"."location" SET "geom" = "new"."geom"
  WHERE ("location"."locationID" = "old"."locationID");
);


--
-- Name: view_occurrence_raw view_occurrence_raw; Type: RULE; Schema: nofa; Owner: -
--

CREATE RULE "view_occurrence_raw" AS
    ON UPDATE TO "nofa"."view_occurrence_raw" DO INSTEAD ( UPDATE "nofa"."occurrence" SET "occurrenceStatus" = "new"."occurrenceStatus"
  WHERE ("occurrence"."occurrenceID" = "old"."occurrenceID");
 UPDATE "nofa"."occurrence" SET "establishmentMeans" = "new"."establishmentMeans"
  WHERE ("occurrence"."occurrenceID" = "old"."occurrenceID");
 UPDATE "nofa"."occurrence" SET "populationTrend" = "new"."populationTrend"
  WHERE ("occurrence"."occurrenceID" = "old"."occurrenceID");
);


--
-- Name: location trigger_match_location2waterbody_insert; Type: TRIGGER; Schema: nofa; Owner: -
--

CREATE TRIGGER "trigger_match_location2waterbody_insert" BEFORE INSERT ON "nofa"."location" FOR EACH ROW EXECUTE PROCEDURE "nofa"."match_location2waterbody"();


--
-- Name: stream updatecentroid; Type: TRIGGER; Schema: nofa; Owner: -
--

CREATE TRIGGER "updatecentroid" BEFORE INSERT OR UPDATE ON "nofa"."stream" FOR EACH ROW EXECUTE PROCEDURE "nofa"."updatecentroid"();


--
-- Name: lake updatecentroid; Type: TRIGGER; Schema: nofa; Owner: -
--

CREATE TRIGGER "updatecentroid" BEFORE INSERT OR UPDATE ON "nofa"."lake" FOR EACH ROW EXECUTE PROCEDURE "nofa"."updatecentroid"();


--
-- Name: waterBody updatedecimallatlon; Type: TRIGGER; Schema: nofa; Owner: -
--

CREATE TRIGGER "updatedecimallatlon" BEFORE INSERT OR UPDATE ON "nofa"."waterBody" FOR EACH ROW EXECUTE PROCEDURE "nofa"."updatedecimallatlon"('centroid');


--
-- Name: event event_fk_datasetID; Type: FK CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."event"
    ADD CONSTRAINT "event_fk_datasetID" FOREIGN KEY ("datasetID") REFERENCES "nofa"."m_dataset"("datasetID") ON UPDATE CASCADE;


--
-- Name: event event_fk_eventDateQualifier; Type: FK CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."event"
    ADD CONSTRAINT "event_fk_eventDateQualifier" FOREIGN KEY ("eventDateQualifier") REFERENCES "nofa"."l_eventDateQualifier"("eventDateQualifier") ON UPDATE CASCADE;


--
-- Name: event event_fk_locationID; Type: FK CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."event"
    ADD CONSTRAINT "event_fk_locationID" FOREIGN KEY ("locationID") REFERENCES "nofa"."location"("locationID") MATCH FULL ON UPDATE CASCADE;


--
-- Name: event event_fk_projectID; Type: FK CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."event"
    ADD CONSTRAINT "event_fk_projectID" FOREIGN KEY ("projectID") REFERENCES "nofa"."m_project"("projectID") ON UPDATE CASCADE;


--
-- Name: event event_fk_referenceID; Type: FK CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."event"
    ADD CONSTRAINT "event_fk_referenceID" FOREIGN KEY ("referenceID") REFERENCES "nofa"."m_reference"("referenceID") ON UPDATE CASCADE;


--
-- Name: event event_fk_reliability; Type: FK CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."event"
    ADD CONSTRAINT "event_fk_reliability" FOREIGN KEY ("reliability") REFERENCES "nofa"."l_reliability"("reliability") MATCH FULL ON UPDATE CASCADE;


--
-- Name: event event_fk_sampleSizeUnit; Type: FK CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."event"
    ADD CONSTRAINT "event_fk_sampleSizeUnit" FOREIGN KEY ("sampleSizeUnit") REFERENCES "nofa"."l_sampleSizeUnit"("sampleSizeUnit") ON UPDATE CASCADE;


--
-- Name: event event_fk_samplingProtocol; Type: FK CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."event"
    ADD CONSTRAINT "event_fk_samplingProtocol" FOREIGN KEY ("samplingProtocol") REFERENCES "nofa"."l_samplingProtocol"("samplingProtocol") ON UPDATE CASCADE;


--
-- Name: l_ecotype l_ecotype_fk_taxonID; Type: FK CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."l_ecotype"
    ADD CONSTRAINT "l_ecotype_fk_taxonID" FOREIGN KEY ("taxonID") REFERENCES "nofa"."l_taxon"("taxonID") MATCH FULL ON UPDATE CASCADE;


--
-- Name: location location_fk_locationType; Type: FK CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."location"
    ADD CONSTRAINT "location_fk_locationType" FOREIGN KEY ("locationType") REFERENCES "nofa"."l_locationType"("locationType") ON UPDATE CASCADE;


--
-- Name: m_reference m_reference_fk_referencetype; Type: FK CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."m_reference"
    ADD CONSTRAINT "m_reference_fk_referencetype" FOREIGN KEY ("referenceType") REFERENCES "nofa"."l_referenceType"("referenceType") ON UPDATE CASCADE;


--
-- Name: m_reference m_reference_fk_rettighetshaver_id; Type: FK CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."m_reference"
    ADD CONSTRAINT "m_reference_fk_rettighetshaver_id" FOREIGN KEY ("dataOwner") REFERENCES "nofa"."l_rettighetshaver"("rettighetshaver_id") ON UPDATE CASCADE;


--
-- Name: mof_occurrence mof_occurrence_fk_measurementType; Type: FK CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."mof_occurrence"
    ADD CONSTRAINT "mof_occurrence_fk_measurementType" FOREIGN KEY ("measurementType") REFERENCES "nofa"."l_mof_occurrence"("measurementType") ON UPDATE CASCADE;


--
-- Name: mof_occurrence mof_occurrence_fk_occurrenceID; Type: FK CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."mof_occurrence"
    ADD CONSTRAINT "mof_occurrence_fk_occurrenceID" FOREIGN KEY ("occurrenceID") REFERENCES "nofa"."occurrence"("occurrenceID") ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: occurrence occurrence_fk_ecotypeID; Type: FK CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."occurrence"
    ADD CONSTRAINT "occurrence_fk_ecotypeID" FOREIGN KEY ("ecotypeID") REFERENCES "nofa"."l_ecotype"("ecotypeID") ON UPDATE CASCADE;


--
-- Name: occurrence occurrence_fk_establishmentMeans; Type: FK CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."occurrence"
    ADD CONSTRAINT "occurrence_fk_establishmentMeans" FOREIGN KEY ("establishmentMeans") REFERENCES "nofa"."l_establishmentMeans"("establishmentMeans") ON UPDATE CASCADE;


--
-- Name: occurrence occurrence_fk_eventID; Type: FK CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."occurrence"
    ADD CONSTRAINT "occurrence_fk_eventID" FOREIGN KEY ("eventID") REFERENCES "nofa"."event"("eventID") ON UPDATE CASCADE;


--
-- Name: occurrence occurrence_fk_lifeStage; Type: FK CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."occurrence"
    ADD CONSTRAINT "occurrence_fk_lifeStage" FOREIGN KEY ("lifeStage") REFERENCES "nofa"."l_lifeStage"("lifeStage") ON UPDATE CASCADE;


--
-- Name: occurrence occurrence_fk_occurrenceStatus; Type: FK CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."occurrence"
    ADD CONSTRAINT "occurrence_fk_occurrenceStatus" FOREIGN KEY ("occurrenceStatus") REFERENCES "nofa"."l_occurrenceStatus"("occurrenceStatus") MATCH FULL ON UPDATE CASCADE;


--
-- Name: occurrence occurrence_fk_populationTrend; Type: FK CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."occurrence"
    ADD CONSTRAINT "occurrence_fk_populationTrend" FOREIGN KEY ("populationTrend") REFERENCES "nofa"."l_populationTrend"("populationTrend") ON UPDATE CASCADE;


--
-- Name: occurrence occurrence_fk_reproductiveCondition; Type: FK CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."occurrence"
    ADD CONSTRAINT "occurrence_fk_reproductiveCondition" FOREIGN KEY ("reproductiveCondition") REFERENCES "nofa"."l_reproductiveCondition"("reproductiveCondition") ON UPDATE CASCADE;


--
-- Name: occurrence occurrence_fk_sex; Type: FK CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."occurrence"
    ADD CONSTRAINT "occurrence_fk_sex" FOREIGN KEY ("sex") REFERENCES "nofa"."l_sex"("sex") ON UPDATE CASCADE;


--
-- Name: occurrence occurrence_fk_spawningCondition; Type: FK CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."occurrence"
    ADD CONSTRAINT "occurrence_fk_spawningCondition" FOREIGN KEY ("spawningCondition") REFERENCES "nofa"."l_spawningCondition"("spawningCondition") ON UPDATE CASCADE;


--
-- Name: occurrence occurrence_fk_spawningLocation; Type: FK CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."occurrence"
    ADD CONSTRAINT "occurrence_fk_spawningLocation" FOREIGN KEY ("spawningLocation") REFERENCES "nofa"."l_spawningLocation"("spawningLocation") ON UPDATE CASCADE;


--
-- Name: occurrence occurrence_fk_taxonID; Type: FK CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."occurrence"
    ADD CONSTRAINT "occurrence_fk_taxonID" FOREIGN KEY ("taxonID") REFERENCES "nofa"."l_taxon"("taxonID") MATCH FULL ON UPDATE CASCADE;


--
-- Name: occurrence occurrence_organismQuantityType_fkey; Type: FK CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."occurrence"
    ADD CONSTRAINT "occurrence_organismQuantityType_fkey" FOREIGN KEY ("organismQuantityType") REFERENCES "nofa"."l_organismQuantityType"("organismQuantityType") ON UPDATE CASCADE;


--
-- Name: samplingTaxaRange samplingTaxaRange_fk_eventID; Type: FK CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."samplingTaxaRange"
    ADD CONSTRAINT "samplingTaxaRange_fk_eventID" FOREIGN KEY ("eventID") REFERENCES "nofa"."event"("eventID") MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: samplingTaxaRange samplingTaxaRange_fk_taxonID; Type: FK CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."samplingTaxaRange"
    ADD CONSTRAINT "samplingTaxaRange_fk_taxonID" FOREIGN KEY ("taxonID") REFERENCES "nofa"."l_taxon"("taxonID") MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: l_taxonSlopeEffect taxonSlopeEffect_fk_taxonID; Type: FK CONSTRAINT; Schema: nofa; Owner: -
--

ALTER TABLE ONLY "nofa"."l_taxonSlopeEffect"
    ADD CONSTRAINT "taxonSlopeEffect_fk_taxonID" FOREIGN KEY ("taxonID") REFERENCES "nofa"."l_taxon"("taxonID") MATCH FULL ON UPDATE CASCADE;


--
-- Name: SCHEMA "nofa"; Type: ACL; Schema: -; Owner: -
--

GRANT ALL ON SCHEMA "nofa" TO "anders";
GRANT ALL ON SCHEMA "nofa" TO "marc";
GRANT USAGE ON SCHEMA "nofa" TO "nofa_reader";


--
-- Name: FUNCTION "updatecentroid"(); Type: ACL; Schema: nofa; Owner: -
--

GRANT ALL ON FUNCTION "nofa"."updatecentroid"() TO "nofa_admin" WITH GRANT OPTION;


--
-- Name: FUNCTION "updatedecimallatlon"(); Type: ACL; Schema: nofa; Owner: -
--

GRANT ALL ON FUNCTION "nofa"."updatedecimallatlon"() TO "nofa_admin" WITH GRANT OPTION;


--
-- Name: TABLE "event"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."event" TO "nofa_reader";
GRANT ALL ON TABLE "nofa"."event" TO "anders";
GRANT ALL ON TABLE "nofa"."event" TO "marc";
GRANT ALL ON TABLE "nofa"."event" TO "nofa_user";
GRANT SELECT ON TABLE "nofa"."event" TO "app_reader";
GRANT SELECT ON TABLE "nofa"."event" TO "app_user";


--
-- Name: TABLE "l_taxonSlopeEffect"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."l_taxonSlopeEffect" TO "nofa_reader";
GRANT SELECT,INSERT,DELETE,TRIGGER,UPDATE ON TABLE "nofa"."l_taxonSlopeEffect" TO "nofa_user";


--
-- Name: TABLE "waterBody"; Type: ACL; Schema: nofa; Owner: -
--

GRANT ALL ON TABLE "nofa"."waterBody" TO "nofa_user";
GRANT SELECT ON TABLE "nofa"."waterBody" TO "nofa_reader";
GRANT SELECT ON TABLE "nofa"."waterBody" TO "app_reader";
GRANT SELECT ON TABLE "nofa"."waterBody" TO "app_user";


--
-- Name: TABLE "lake"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."lake" TO "nofa_reader";
GRANT ALL ON TABLE "nofa"."lake" TO "nofa_user";
GRANT SELECT ON TABLE "nofa"."lake" TO "app_reader";
GRANT SELECT ON TABLE "nofa"."lake" TO "app_user";


--
-- Name: TABLE "location"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."location" TO "nofa_reader";
GRANT ALL ON TABLE "nofa"."location" TO "nofa_user";
GRANT SELECT ON TABLE "nofa"."location" TO "app_reader";
GRANT SELECT ON TABLE "nofa"."location" TO "app_user";
GRANT ALL ON TABLE "nofa"."location" TO "nofa_admin" WITH GRANT OPTION;


--
-- Name: TABLE "occurrence"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."occurrence" TO "nofa_reader";
GRANT ALL ON TABLE "nofa"."occurrence" TO "anders";
GRANT ALL ON TABLE "nofa"."occurrence" TO "nofa_user";
GRANT SELECT ON TABLE "nofa"."occurrence" TO "app_reader";
GRANT SELECT ON TABLE "nofa"."occurrence" TO "app_user";


--
-- Name: TABLE "m_project"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."m_project" TO "nofa_reader";
GRANT ALL ON TABLE "nofa"."m_project" TO "anders";
GRANT ALL ON TABLE "nofa"."m_project" TO "nofa_user";
GRANT SELECT ON TABLE "nofa"."m_project" TO "app_reader";
GRANT SELECT ON TABLE "nofa"."m_project" TO "app_user";


--
-- Name: SEQUENCE "a_prosjekt_prosjekt_id_seq"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON SEQUENCE "nofa"."a_prosjekt_prosjekt_id_seq" TO "nofa_reader";
GRANT ALL ON SEQUENCE "nofa"."a_prosjekt_prosjekt_id_seq" TO "nofa_user";
GRANT SELECT ON SEQUENCE "nofa"."a_prosjekt_prosjekt_id_seq" TO "app_reader";
GRANT SELECT ON SEQUENCE "nofa"."a_prosjekt_prosjekt_id_seq" TO "app_user";


--
-- Name: TABLE "m_reference"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."m_reference" TO "nofa_reader";
GRANT ALL ON TABLE "nofa"."m_reference" TO "anders";
GRANT ALL ON TABLE "nofa"."m_reference" TO "nofa_user";
GRANT SELECT ON TABLE "nofa"."m_reference" TO "app_reader";
GRANT SELECT ON TABLE "nofa"."m_reference" TO "app_user";


--
-- Name: SEQUENCE "a_referanse_referanse_id_seq"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON SEQUENCE "nofa"."a_referanse_referanse_id_seq" TO "nofa_reader";
GRANT ALL ON SEQUENCE "nofa"."a_referanse_referanse_id_seq" TO "nofa_user";
GRANT SELECT ON SEQUENCE "nofa"."a_referanse_referanse_id_seq" TO "app_reader";
GRANT SELECT ON SEQUENCE "nofa"."a_referanse_referanse_id_seq" TO "app_user";


--
-- Name: TABLE "m_dataset"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."m_dataset" TO "nofa_reader";
GRANT ALL ON TABLE "nofa"."m_dataset" TO "nofa_user";
GRANT SELECT ON TABLE "nofa"."m_dataset" TO "app_reader";
GRANT SELECT ON TABLE "nofa"."m_dataset" TO "app_user";


--
-- Name: TABLE "dwc_a_eventcore_emof"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."dwc_a_eventcore_emof" TO "nofa_reader";
GRANT ALL ON TABLE "nofa"."dwc_a_eventcore_emof" TO "nofa_admin" WITH GRANT OPTION;


--
-- Name: TABLE "dwc_a_eventcore_event"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."dwc_a_eventcore_event" TO "nofa_reader";
GRANT ALL ON TABLE "nofa"."dwc_a_eventcore_event" TO "nofa_admin" WITH GRANT OPTION;


--
-- Name: TABLE "l_taxon"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."l_taxon" TO "nofa_reader";
GRANT ALL ON TABLE "nofa"."l_taxon" TO "nofa_user";
GRANT ALL ON TABLE "nofa"."l_taxon" TO "nofa_admin" WITH GRANT OPTION;
SET SESSION AUTHORIZATION "nofa_admin";
GRANT SELECT,INSERT,UPDATE ON TABLE "nofa"."l_taxon" TO "nofa_user";
RESET SESSION AUTHORIZATION;
GRANT SELECT ON TABLE "nofa"."l_taxon" TO "app_reader";
GRANT SELECT ON TABLE "nofa"."l_taxon" TO "app_user";


--
-- Name: TABLE "dwc_a_eventcore_occurrence"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."dwc_a_eventcore_occurrence" TO "nofa_reader";
GRANT ALL ON TABLE "nofa"."dwc_a_eventcore_occurrence" TO "nofa_admin" WITH GRANT OPTION;


--
-- Name: TABLE "input_locations"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."input_locations" TO "nofa_reader";
GRANT ALL ON TABLE "nofa"."input_locations" TO "nofa_admin";


--
-- Name: TABLE "l_basisOfRecord"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."l_basisOfRecord" TO "nofa_reader";
GRANT ALL ON TABLE "nofa"."l_basisOfRecord" TO "nofa_user";
GRANT SELECT ON TABLE "nofa"."l_basisOfRecord" TO "app_reader";
GRANT SELECT ON TABLE "nofa"."l_basisOfRecord" TO "app_user";
GRANT ALL ON TABLE "nofa"."l_basisOfRecord" TO "nofa_admin" WITH GRANT OPTION;


--
-- Name: TABLE "l_ecotype"; Type: ACL; Schema: nofa; Owner: -
--

GRANT ALL ON TABLE "nofa"."l_ecotype" TO "nofa_admin" WITH GRANT OPTION;
GRANT SELECT ON TABLE "nofa"."l_ecotype" TO "nofa_reader";
GRANT ALL ON TABLE "nofa"."l_ecotype" TO "nofa_user";
SET SESSION AUTHORIZATION "nofa_admin";
GRANT SELECT,INSERT,UPDATE ON TABLE "nofa"."l_ecotype" TO "nofa_user";
RESET SESSION AUTHORIZATION;
GRANT SELECT ON TABLE "nofa"."l_ecotype" TO "app_reader";
GRANT SELECT ON TABLE "nofa"."l_ecotype" TO "app_user";


--
-- Name: TABLE "l_establishmentMeans"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."l_establishmentMeans" TO "nofa_reader";
GRANT ALL ON TABLE "nofa"."l_establishmentMeans" TO "anders";
GRANT ALL ON TABLE "nofa"."l_establishmentMeans" TO "nofa_user";
GRANT SELECT ON TABLE "nofa"."l_establishmentMeans" TO "app_reader";
GRANT SELECT ON TABLE "nofa"."l_establishmentMeans" TO "app_user";


--
-- Name: TABLE "l_eventDateQualifier"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."l_eventDateQualifier" TO "app_reader";
GRANT SELECT ON TABLE "nofa"."l_eventDateQualifier" TO "app_user";
GRANT ALL ON TABLE "nofa"."l_eventDateQualifier" TO "nofa_user";
GRANT ALL ON TABLE "nofa"."l_eventDateQualifier" TO "nofa_admin" WITH GRANT OPTION;
GRANT SELECT ON TABLE "nofa"."l_eventDateQualifier" TO "nofa_reader";


--
-- Name: TABLE "l_lifeStage"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."l_lifeStage" TO "nofa_reader";
GRANT ALL ON TABLE "nofa"."l_lifeStage" TO "nofa_user";
GRANT SELECT ON TABLE "nofa"."l_lifeStage" TO "app_reader";
GRANT SELECT ON TABLE "nofa"."l_lifeStage" TO "app_user";


--
-- Name: TABLE "l_locationType"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."l_locationType" TO "nofa_reader";
GRANT ALL ON TABLE "nofa"."l_locationType" TO "nofa_user";
GRANT SELECT ON TABLE "nofa"."l_locationType" TO "app_reader";
GRANT SELECT ON TABLE "nofa"."l_locationType" TO "app_user";
GRANT ALL ON TABLE "nofa"."l_locationType" TO "nofa_admin" WITH GRANT OPTION;


--
-- Name: TABLE "l_mof_occurrence"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."l_mof_occurrence" TO "nofa_reader";
GRANT ALL ON TABLE "nofa"."l_mof_occurrence" TO "nofa_user";
GRANT SELECT ON TABLE "nofa"."l_mof_occurrence" TO "app_reader";
GRANT SELECT ON TABLE "nofa"."l_mof_occurrence" TO "app_user";


--
-- Name: TABLE "l_occurrenceStatus"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."l_occurrenceStatus" TO "app_reader";
GRANT SELECT ON TABLE "nofa"."l_occurrenceStatus" TO "app_user";
GRANT ALL ON TABLE "nofa"."l_occurrenceStatus" TO "nofa_user";
GRANT ALL ON TABLE "nofa"."l_occurrenceStatus" TO "nofa_admin" WITH GRANT OPTION;
GRANT SELECT ON TABLE "nofa"."l_occurrenceStatus" TO "nofa_reader";


--
-- Name: TABLE "l_organismQuantityType"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."l_organismQuantityType" TO "nofa_reader";
GRANT ALL ON TABLE "nofa"."l_organismQuantityType" TO "nofa_admin" WITH GRANT OPTION;


--
-- Name: TABLE "l_populationTrend"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."l_populationTrend" TO "app_reader";
GRANT SELECT ON TABLE "nofa"."l_populationTrend" TO "app_user";
GRANT ALL ON TABLE "nofa"."l_populationTrend" TO "nofa_user";
GRANT ALL ON TABLE "nofa"."l_populationTrend" TO "nofa_admin" WITH GRANT OPTION;
GRANT SELECT ON TABLE "nofa"."l_populationTrend" TO "nofa_reader";


--
-- Name: SEQUENCE "l_populationTrend_populationTrendID_seq"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON SEQUENCE "nofa"."l_populationTrend_populationTrendID_seq" TO "app_reader";
GRANT SELECT ON SEQUENCE "nofa"."l_populationTrend_populationTrendID_seq" TO "app_user";
GRANT ALL ON SEQUENCE "nofa"."l_populationTrend_populationTrendID_seq" TO "nofa_user";
GRANT ALL ON SEQUENCE "nofa"."l_populationTrend_populationTrendID_seq" TO "nofa_admin" WITH GRANT OPTION;
GRANT SELECT ON SEQUENCE "nofa"."l_populationTrend_populationTrendID_seq" TO "nofa_reader";


--
-- Name: TABLE "l_referenceType"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."l_referenceType" TO "nofa_reader";
GRANT ALL ON TABLE "nofa"."l_referenceType" TO "anders";
GRANT ALL ON TABLE "nofa"."l_referenceType" TO "nofa_user";
GRANT SELECT ON TABLE "nofa"."l_referenceType" TO "app_reader";
GRANT SELECT ON TABLE "nofa"."l_referenceType" TO "app_user";


--
-- Name: TABLE "l_reliability"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."l_reliability" TO "app_reader";
GRANT SELECT ON TABLE "nofa"."l_reliability" TO "app_user";
GRANT ALL ON TABLE "nofa"."l_reliability" TO "nofa_user";
GRANT ALL ON TABLE "nofa"."l_reliability" TO "nofa_admin" WITH GRANT OPTION;
GRANT SELECT ON TABLE "nofa"."l_reliability" TO "nofa_reader";


--
-- Name: TABLE "l_reproductiveCondition"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."l_reproductiveCondition" TO "nofa_reader";
GRANT ALL ON TABLE "nofa"."l_reproductiveCondition" TO "nofa_user";
GRANT SELECT ON TABLE "nofa"."l_reproductiveCondition" TO "app_reader";
GRANT SELECT ON TABLE "nofa"."l_reproductiveCondition" TO "app_user";


--
-- Name: TABLE "l_rettighetshaver"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."l_rettighetshaver" TO "nofa_reader";
GRANT ALL ON TABLE "nofa"."l_rettighetshaver" TO "anders";
GRANT ALL ON TABLE "nofa"."l_rettighetshaver" TO "nofa_user";
GRANT SELECT ON TABLE "nofa"."l_rettighetshaver" TO "app_reader";
GRANT SELECT ON TABLE "nofa"."l_rettighetshaver" TO "app_user";


--
-- Name: TABLE "l_sampleSizeUnit"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."l_sampleSizeUnit" TO "nofa_reader";
GRANT ALL ON TABLE "nofa"."l_sampleSizeUnit" TO "nofa_user";
GRANT SELECT ON TABLE "nofa"."l_sampleSizeUnit" TO "app_reader";
GRANT SELECT ON TABLE "nofa"."l_sampleSizeUnit" TO "app_user";


--
-- Name: TABLE "l_samplingProtocol"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."l_samplingProtocol" TO "nofa_reader";
GRANT ALL ON TABLE "nofa"."l_samplingProtocol" TO "anders";
GRANT ALL ON TABLE "nofa"."l_samplingProtocol" TO "nofa_user";
GRANT SELECT ON TABLE "nofa"."l_samplingProtocol" TO "app_reader";
GRANT SELECT ON TABLE "nofa"."l_samplingProtocol" TO "app_user";


--
-- Name: TABLE "l_sex"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."l_sex" TO "nofa_reader";
GRANT ALL ON TABLE "nofa"."l_sex" TO "nofa_user";
GRANT SELECT ON TABLE "nofa"."l_sex" TO "app_reader";
GRANT SELECT ON TABLE "nofa"."l_sex" TO "app_user";


--
-- Name: TABLE "l_spawningCondition"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."l_spawningCondition" TO "nofa_reader";
GRANT ALL ON TABLE "nofa"."l_spawningCondition" TO "anders";
GRANT ALL ON TABLE "nofa"."l_spawningCondition" TO "nofa_user";
GRANT SELECT ON TABLE "nofa"."l_spawningCondition" TO "app_reader";
GRANT SELECT ON TABLE "nofa"."l_spawningCondition" TO "app_user";


--
-- Name: TABLE "l_spawningLocation"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."l_spawningLocation" TO "nofa_reader";
GRANT ALL ON TABLE "nofa"."l_spawningLocation" TO "anders";
GRANT ALL ON TABLE "nofa"."l_spawningLocation" TO "nofa_user";
GRANT SELECT ON TABLE "nofa"."l_spawningLocation" TO "app_reader";
GRANT SELECT ON TABLE "nofa"."l_spawningLocation" TO "app_user";


--
-- Name: TABLE "l_taxon_historicDistribution"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."l_taxon_historicDistribution" TO "nofa_reader";
GRANT SELECT,INSERT,DELETE,TRIGGER,UPDATE ON TABLE "nofa"."l_taxon_historicDistribution" TO "nofa_user";


--
-- Name: SEQUENCE "lakeID_seq"; Type: ACL; Schema: nofa; Owner: -
--

GRANT ALL ON SEQUENCE "nofa"."lakeID_seq" TO "nofa_admin" WITH GRANT OPTION;
GRANT SELECT ON SEQUENCE "nofa"."lakeID_seq" TO "nofa_reader";
GRANT ALL ON SEQUENCE "nofa"."lakeID_seq" TO "nofa_user";
GRANT SELECT ON SEQUENCE "nofa"."lakeID_seq" TO "app_reader";
GRANT SELECT ON SEQUENCE "nofa"."lakeID_seq" TO "app_user";


--
-- Name: SEQUENCE "location_id_seq"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON SEQUENCE "nofa"."location_id_seq" TO "nofa_reader";
GRANT ALL ON SEQUENCE "nofa"."location_id_seq" TO "nofa_user";
GRANT SELECT ON SEQUENCE "nofa"."location_id_seq" TO "app_reader";
GRANT SELECT ON SEQUENCE "nofa"."location_id_seq" TO "app_user";


--
-- Name: TABLE "mapping_info"; Type: ACL; Schema: nofa; Owner: -
--

GRANT ALL ON TABLE "nofa"."mapping_info" TO "nofa_admin" WITH GRANT OPTION;
GRANT SELECT ON TABLE "nofa"."mapping_info" TO "nofa_reader";
GRANT ALL ON TABLE "nofa"."mapping_info" TO "nofa_user";
GRANT SELECT ON TABLE "nofa"."mapping_info" TO "app_reader";
GRANT SELECT ON TABLE "nofa"."mapping_info" TO "app_user";


--
-- Name: TABLE "mof_occurrence"; Type: ACL; Schema: nofa; Owner: -
--

GRANT ALL ON TABLE "nofa"."mof_occurrence" TO "nofa_user";
GRANT SELECT ON TABLE "nofa"."mof_occurrence" TO "nofa_reader";
GRANT SELECT ON TABLE "nofa"."mof_occurrence" TO "app_reader";
GRANT SELECT ON TABLE "nofa"."mof_occurrence" TO "app_user";


--
-- Name: TABLE "number_populations_5000m_pike"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."number_populations_5000m_pike" TO "nofa_reader";
GRANT SELECT,INSERT,DELETE,TRIGGER,UPDATE ON TABLE "nofa"."number_populations_5000m_pike" TO "nofa_user";


--
-- Name: TABLE "otherwaterbody"; Type: ACL; Schema: nofa; Owner: -
--

GRANT ALL ON TABLE "nofa"."otherwaterbody" TO "anders";
GRANT ALL ON TABLE "nofa"."otherwaterbody" TO "nofa_user";
GRANT SELECT ON TABLE "nofa"."otherwaterbody" TO "nofa_reader";
GRANT SELECT ON TABLE "nofa"."otherwaterbody" TO "app_reader";
GRANT SELECT ON TABLE "nofa"."otherwaterbody" TO "app_user";


--
-- Name: TABLE "samplingTaxaRange"; Type: ACL; Schema: nofa; Owner: -
--

GRANT ALL ON TABLE "nofa"."samplingTaxaRange" TO "anders";
GRANT ALL ON TABLE "nofa"."samplingTaxaRange" TO "nofa_user";
GRANT SELECT ON TABLE "nofa"."samplingTaxaRange" TO "nofa_reader";
GRANT SELECT ON TABLE "nofa"."samplingTaxaRange" TO "app_reader";
GRANT SELECT ON TABLE "nofa"."samplingTaxaRange" TO "app_user";


--
-- Name: TABLE "simulation_out_test"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."simulation_out_test" TO "nofa_reader";
GRANT ALL ON TABLE "nofa"."simulation_out_test" TO "nofa_admin";


--
-- Name: TABLE "stream"; Type: ACL; Schema: nofa; Owner: -
--

GRANT ALL ON TABLE "nofa"."stream" TO "anders";
GRANT ALL ON TABLE "nofa"."stream" TO "nofa_user";
GRANT SELECT ON TABLE "nofa"."stream" TO "nofa_reader";
GRANT SELECT ON TABLE "nofa"."stream" TO "app_reader";
GRANT SELECT ON TABLE "nofa"."stream" TO "app_user";


--
-- Name: TABLE "temp_m_dataset_data_import"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."temp_m_dataset_data_import" TO "nofa_reader";
GRANT ALL ON TABLE "nofa"."temp_m_dataset_data_import" TO "nofa_admin";


--
-- Name: TABLE "view_aggregated_occurrence"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."view_aggregated_occurrence" TO "nofa_reader";
GRANT ALL ON TABLE "nofa"."view_aggregated_occurrence" TO "nofa_admin" WITH GRANT OPTION;


--
-- Name: TABLE "view_introductions"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."view_introductions" TO "nofa_reader";


--
-- Name: TABLE "view_lake_environment"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."view_lake_environment" TO "nofa_reader";
GRANT SELECT,INSERT,DELETE,TRIGGER,UPDATE ON TABLE "nofa"."view_lake_environment" TO "nofa_user";


--
-- Name: TABLE "view_location_environment"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."view_location_environment" TO "nofa_reader";
GRANT SELECT,INSERT,DELETE,TRIGGER,UPDATE ON TABLE "nofa"."view_location_environment" TO "nofa_user";


--
-- Name: TABLE "view_locations_raw"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."view_locations_raw" TO "nofa_reader";
GRANT ALL ON TABLE "nofa"."view_locations_raw" TO "nofa_admin";


--
-- Name: TABLE "view_occurrence_all"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."view_occurrence_all" TO "nofa_reader";
GRANT ALL ON TABLE "nofa"."view_occurrence_all" TO "nofa_admin" WITH GRANT OPTION;


--
-- Name: TABLE "view_occurrence_by_event"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."view_occurrence_by_event" TO "nofa_reader";
GRANT SELECT,INSERT,DELETE,TRIGGER,UPDATE ON TABLE "nofa"."view_occurrence_by_event" TO "nofa_user";
GRANT ALL ON TABLE "nofa"."view_occurrence_by_event" TO "anders";


--
-- Name: TABLE "view_occurrence_by_event_all"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."view_occurrence_by_event_all" TO "nofa_reader";
GRANT ALL ON TABLE "nofa"."view_occurrence_by_event_all" TO "anders";
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "nofa"."view_occurrence_by_event_all" TO "nofa_user";


--
-- Name: TABLE "view_occurrence_by_event_all_samplingprotocol"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."view_occurrence_by_event_all_samplingprotocol" TO "nofa_reader";
GRANT SELECT,INSERT,DELETE,TRIGGER,UPDATE ON TABLE "nofa"."view_occurrence_by_event_all_samplingprotocol" TO "nofa_user";
GRANT ALL ON TABLE "nofa"."view_occurrence_by_event_all_samplingprotocol" TO "anders";


--
-- Name: TABLE "view_occurrence_pike"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."view_occurrence_pike" TO "nofa_reader";
GRANT ALL ON TABLE "nofa"."view_occurrence_pike" TO "nofa_admin" WITH GRANT OPTION;


--
-- Name: TABLE "view_occurrence_raw"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."view_occurrence_raw" TO "nofa_reader";
GRANT ALL ON TABLE "nofa"."view_occurrence_raw" TO "postgres";


--
-- Name: TABLE "view_occurrence_roach"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."view_occurrence_roach" TO "nofa_reader";
GRANT ALL ON TABLE "nofa"."view_occurrence_roach" TO "nofa_admin" WITH GRANT OPTION;


--
-- Name: TABLE "view_occurrences_agr_by_location"; Type: ACL; Schema: nofa; Owner: -
--

GRANT SELECT ON TABLE "nofa"."view_occurrences_agr_by_location" TO "nofa_reader";
GRANT ALL ON TABLE "nofa"."view_occurrences_agr_by_location" TO "anders";
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "nofa"."view_occurrences_agr_by_location" TO "nofa_user";


--
-- Name: TABLE "dataset_log"; Type: ACL; Schema: plugin; Owner: -
--

GRANT SELECT ON TABLE "plugin"."dataset_log" TO "nofa_reader";
GRANT ALL ON TABLE "plugin"."dataset_log" TO "anders";
GRANT ALL ON TABLE "plugin"."dataset_log" TO "nofa_user";
GRANT SELECT ON TABLE "plugin"."dataset_log" TO "app_reader";
GRANT SELECT ON TABLE "plugin"."dataset_log" TO "app_user";


--
-- Name: TABLE "event_log"; Type: ACL; Schema: plugin; Owner: -
--

GRANT SELECT ON TABLE "plugin"."event_log" TO "nofa_reader";
GRANT ALL ON TABLE "plugin"."event_log" TO "anders";
GRANT ALL ON TABLE "plugin"."event_log" TO "nofa_user";
GRANT SELECT ON TABLE "plugin"."event_log" TO "app_reader";
GRANT SELECT ON TABLE "plugin"."event_log" TO "app_user";


--
-- Name: TABLE "location_log"; Type: ACL; Schema: plugin; Owner: -
--

GRANT SELECT ON TABLE "plugin"."location_log" TO "nofa_reader";
GRANT ALL ON TABLE "plugin"."location_log" TO "anders";
GRANT ALL ON TABLE "plugin"."location_log" TO "nofa_user";
GRANT SELECT ON TABLE "plugin"."location_log" TO "app_reader";
GRANT SELECT ON TABLE "plugin"."location_log" TO "app_user";


--
-- Name: TABLE "occurrence_log"; Type: ACL; Schema: plugin; Owner: -
--

GRANT SELECT ON TABLE "plugin"."occurrence_log" TO "nofa_reader";
GRANT ALL ON TABLE "plugin"."occurrence_log" TO "anders";
GRANT ALL ON TABLE "plugin"."occurrence_log" TO "nofa_user";
GRANT SELECT ON TABLE "plugin"."occurrence_log" TO "app_reader";
GRANT SELECT ON TABLE "plugin"."occurrence_log" TO "app_user";


--
-- Name: SEQUENCE "occurrence_log_occurrence_log_id_seq"; Type: ACL; Schema: plugin; Owner: -
--

GRANT ALL ON SEQUENCE "plugin"."occurrence_log_occurrence_log_id_seq" TO "app_user";
GRANT ALL ON SEQUENCE "plugin"."occurrence_log_occurrence_log_id_seq" TO "nofa_user";
GRANT SELECT,USAGE ON SEQUENCE "plugin"."occurrence_log_occurrence_log_id_seq" TO "app_reader";
GRANT SELECT,USAGE ON SEQUENCE "plugin"."occurrence_log_occurrence_log_id_seq" TO "nofa_reader";
GRANT SELECT,USAGE ON SEQUENCE "plugin"."occurrence_log_occurrence_log_id_seq" TO "nofa_guest";


--
-- Name: TABLE "project_log"; Type: ACL; Schema: plugin; Owner: -
--

GRANT SELECT ON TABLE "plugin"."project_log" TO "nofa_reader";
GRANT ALL ON TABLE "plugin"."project_log" TO "anders";
GRANT ALL ON TABLE "plugin"."project_log" TO "nofa_user";
GRANT SELECT ON TABLE "plugin"."project_log" TO "app_reader";
GRANT SELECT ON TABLE "plugin"."project_log" TO "app_user";


--
-- Name: TABLE "reference_log"; Type: ACL; Schema: plugin; Owner: -
--

GRANT SELECT ON TABLE "plugin"."reference_log" TO "nofa_reader";
GRANT ALL ON TABLE "plugin"."reference_log" TO "anders";
GRANT ALL ON TABLE "plugin"."reference_log" TO "nofa_user";
GRANT SELECT ON TABLE "plugin"."reference_log" TO "app_reader";
GRANT SELECT ON TABLE "plugin"."reference_log" TO "app_user";


--
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: nofa; Owner: -
--

ALTER DEFAULT PRIVILEGES FOR ROLE "anders" IN SCHEMA "nofa" REVOKE ALL ON TABLES  FROM "anders";
ALTER DEFAULT PRIVILEGES FOR ROLE "anders" IN SCHEMA "nofa" GRANT ALL ON TABLES  TO "nofa_admin";
ALTER DEFAULT PRIVILEGES FOR ROLE "anders" IN SCHEMA "nofa" GRANT SELECT ON TABLES  TO "nofa_reader";


--
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: nofa; Owner: -
--

ALTER DEFAULT PRIVILEGES FOR ROLE "stefan" IN SCHEMA "nofa" REVOKE ALL ON TABLES  FROM "stefan";
ALTER DEFAULT PRIVILEGES FOR ROLE "stefan" IN SCHEMA "nofa" GRANT SELECT ON TABLES  TO "nofa_reader";
ALTER DEFAULT PRIVILEGES FOR ROLE "stefan" IN SCHEMA "nofa" GRANT SELECT,INSERT,DELETE,TRIGGER,UPDATE ON TABLES  TO "nofa_user";


--
-- PostgreSQL database dump complete
--

